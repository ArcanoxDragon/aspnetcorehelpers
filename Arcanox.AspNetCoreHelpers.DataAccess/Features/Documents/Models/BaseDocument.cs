﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Models
{
	public class BaseDocument
	{
		public static void RegisterDefaults<TDocument>( EntityTypeBuilder<TDocument> entity )
			where TDocument : BaseDocument
		{
			entity.HasKey( e => e.Id );

			entity.HasIndex( e => e.Name ).IsUnique();
			entity.Property( e => e.Name ).IsRequired();

			entity.Property( e => e.DateCreated )
				  .HasColumnType( "datetime" )
			      .HasDefaultValueSql( "CURRENT_TIMESTAMP" );

			entity.Property( e => e.LastModifiedAt )
				  .HasColumnType( "datetime" );
		}

		public int       Id             { get; set; }
		public string    Author         { get; set; }
		public string    Name           { get; set; }
		public string    Title          { get; set; }
		public DateTime? DateCreated    { get; set; }
		public DateTime? LastModifiedAt { get; set; }
		public string    LastModifiedBy { get; set; }
	}
}