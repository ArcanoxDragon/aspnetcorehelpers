﻿using System;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Managers;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Models;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Options;
using Microsoft.Extensions.Options;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Services
{
	public class DocumentService<TDocument> where TDocument : BaseDocument, new()
	{
		private readonly DocumentOptions<TDocument>  options;
		private readonly IDocumentManager<TDocument> manager;

		public DocumentService( IOptionsSnapshot<DocumentOptions<TDocument>> options,
								IDocumentManager<TDocument> manager )
		{
			this.options = options.Value;
			this.manager = manager;
		}

		public TDocument Get( string name )
		{
			var document = this.manager.ByName( name );

			if ( document == null && this.options.CreateIfNoExist )
			{
				document = new TDocument {
					Author      = this.options.DefaultAuthor,
					DateCreated = DateTime.Now,
					Name        = name,
				};

				this.options.OnCreate?.Invoke( document );
				this.manager.Create( document );
			}

			return document;
		}

		public async Task<TDocument> GetAsync( string name )
		{
			var document = await this.manager.ByNameAsync( name );

			if ( document == null && this.options.CreateIfNoExist )
			{
				document = new TDocument {
					Author      = this.options.DefaultAuthor,
					DateCreated = DateTime.Now,
					Name        = name,
				};

				this.options.OnCreate?.Invoke( document );
				await this.manager.CreateAsync( document );
			}

			return document;
		}
	}
}