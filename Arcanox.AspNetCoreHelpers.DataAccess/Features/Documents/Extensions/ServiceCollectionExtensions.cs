﻿using System;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Managers;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Models;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Options;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddDocuments<TDocument, TManager>( this IServiceCollection services, Action<DocumentOptions<TDocument>> configure )
			where TDocument : BaseDocument, new()
			where TManager : class, IDocumentManager<TDocument>
		{
			services.Configure( configure );
			services.AddTransient<IDocumentManager<TDocument>, TManager>();
			services.AddScoped<DocumentService<TDocument>>();

			return services;
		}

		public static IServiceCollection AddDocuments<TDocument, TManager>( this IServiceCollection services )
			where TDocument : BaseDocument, new()
			where TManager : class, IDocumentManager<TDocument>
			=> services.AddDocuments<TDocument, TManager>( options => { } );

		public static IServiceCollection AddDocuments<TDocument>( this IServiceCollection services, Action<DocumentOptions<TDocument>> configure )
			where TDocument : BaseDocument, new()
			=> services.AddDocuments<TDocument, BaseDocumentManager<TDocument>>( configure );

		public static IServiceCollection AddDocuments<TDocument>( this IServiceCollection services )
			where TDocument : BaseDocument, new()
			=> services.AddDocuments<TDocument, BaseDocumentManager<TDocument>>();
	}
}