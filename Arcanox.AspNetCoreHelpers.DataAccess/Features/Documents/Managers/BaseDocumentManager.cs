﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.DataAccess.Abstract;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Models;
using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Managers
{
	public class BaseDocumentManager<TDocument> : BaseManager<TDocument, int>, IDocumentManager<TDocument>
		where TDocument : BaseDocument
	{
		public BaseDocumentManager( Workspace workspace ) : base( workspace ) { }
		public BaseDocumentManager( DbContext dataContext ) : base( dataContext ) { }

		protected override Expression<Func<TDocument, int>> KeyExpression => doc => doc.Id;

		public IQueryable<TDocument> Documents => this.Set;

		public TDocument ByName( string name ) => this.Documents.SingleOrDefault( d => d.Name == name );
		public Task<TDocument> ByNameAsync( string name ) => this.Documents.SingleOrDefaultAsync( d => d.Name == name );
	}
}