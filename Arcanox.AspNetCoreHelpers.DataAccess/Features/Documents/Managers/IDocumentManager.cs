﻿using System.Linq;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Models;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Managers
{
	public interface IDocumentManager<TDocument> where TDocument : BaseDocument
	{
		IQueryable<TDocument> Documents { get; }

		TDocument ByName( string name );
		Task<TDocument> ByNameAsync( string name );

		TDocument Create( TDocument model );
		Task<TDocument> CreateAsync( TDocument model );
	}
}