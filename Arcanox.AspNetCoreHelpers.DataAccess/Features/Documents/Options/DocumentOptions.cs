﻿using System;
using Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Models;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Features.Documents.Options
{
	public class DocumentOptions<TDocument> where TDocument : BaseDocument
	{
		public bool              CreateIfNoExist { get; set; } = true;
		public string            DefaultAuthor   { get; set; } = "System";
		public Action<TDocument> OnCreate        { get; set; }
	}
}