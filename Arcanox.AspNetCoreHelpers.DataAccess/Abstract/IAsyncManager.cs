﻿using System;
using System.Threading.Tasks;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Abstract
{
	public interface IAsyncManager<TModel, in TKey> where TModel : class
	{
		Task<TModel> CreateAsync( TModel model );
		Task<TModel> GetAsync( TKey key );
		Task UpdateAsync( TModel model );
		Task MakeChangesAsync( TModel model, Action<TModel> workFunction );
		Task DeleteAsync( TModel model );
	}
}