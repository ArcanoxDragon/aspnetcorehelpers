﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Query.SqlExpressions;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Abstract
{
	public interface IDbFunctionTranslator
	{
		SqlExpression Translate( IReadOnlyCollection<SqlExpression> parameters );
	}
}