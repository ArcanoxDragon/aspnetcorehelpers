﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.DataAccess.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Abstract
{
	public abstract class BaseManager<TModel, TKey>
		: IManager<TModel, TKey>, IAsyncManager<TModel, TKey>
		where TModel : class
	{
		protected BaseManager( Workspace workspace )
		{
			this.Workspace = workspace;
		}

		// For easier migration
		protected BaseManager( DbContext dataContext )
		{
			this.Workspace = new Workspace( dataContext );
		}

		protected Workspace Workspace { get; }

		protected virtual bool SetIsNoTracking => false;

		protected virtual IQueryable<TModel> Set
			=> this.SetIsNoTracking
				   ? this.Workspace.DataContext.Set<TModel>().AsNoTracking()
				   : this.Workspace.DataContext.Set<TModel>();

		protected abstract Expression<Func<TModel, TKey>> KeyExpression { get; }

		private Expression<Func<TModel, bool>> GetKeyPredicate( TKey key )
		{
			var keyAccessBody = this.KeyExpression.Body;

			if ( !( keyAccessBody is MemberExpression keyAccessExpression ) )
				throw new InvalidOperationException( $"Invalid key expression \"{keyAccessBody}\"" );

			var criteriaExpression        = Expression.Constant( key, typeof( TKey ) );
			var parameterExpression       = Expression.Parameter( typeof( TModel ), "m" );
			var parameterAccessExpression = Expression.MakeMemberAccess( parameterExpression, keyAccessExpression.Member );
			var equalsExpression          = Expression.Equal( parameterAccessExpression, criteriaExpression );

			return Expression.Lambda<Func<TModel, bool>>( equalsExpression, parameterExpression );
		}

		#region Synchronous

		public virtual TModel Create( TModel model )
		{
			var entry = this.Workspace.UnitOfWork( uow => this.Workspace.DataContext.Add( model ) );

			return entry.Entity;
		}

		public virtual TModel Get( TKey key ) => this.Set.SingleOrDefault( this.GetKeyPredicate( key ) );
		public virtual void Update( TModel model ) => this.Workspace.UnitOfWork( uow => this.Workspace.DataContext.Update( model ) );
		public virtual void MakeChanges( TModel model, Action<TModel> workFunction ) => this.Workspace.MakeChanges( model, workFunction );
		public virtual void Delete( TModel model ) => this.Workspace.UnitOfWork( uow => this.Workspace.DataContext.Remove( model ) );

		#endregion Synchronous

		#region Asynchronous

		public virtual async Task<TModel> CreateAsync( TModel model )
		{
			var entry = await this.Workspace.UnitOfWorkAsync( async uow => await this.Workspace.DataContext.AddAsync( model ) );

			return entry.Entity;
		}

		public virtual Task<TModel> GetAsync( TKey key ) => this.Set.SingleOrDefaultAsync( this.GetKeyPredicate( key ) );
		public virtual Task UpdateAsync( TModel model ) => this.Workspace.UnitOfWorkAsync( uow => this.Workspace.DataContext.Update( model ) );
		public virtual Task MakeChangesAsync( TModel model, Action<TModel> workFunction ) => this.Workspace.MakeChangesAsync( model, workFunction );
		public virtual Task DeleteAsync( TModel model ) => this.Workspace.UnitOfWorkAsync( uow => this.Workspace.DataContext.Remove( model ) );

		#endregion
	}
}