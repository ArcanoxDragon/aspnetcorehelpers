﻿using System;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Abstract
{
	public interface IManager<TModel, in TKey> where TModel : class
	{
		TModel Create( TModel model );
		TModel Get( TKey key );
		void Update( TModel model );
		void MakeChanges( TModel model, Action<TModel> workFunction );
		void Delete( TModel model );
	}
}