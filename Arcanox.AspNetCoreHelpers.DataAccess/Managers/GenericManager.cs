﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Arcanox.AspNetCoreHelpers.DataAccess.Abstract;
using Arcanox.AspNetCoreHelpers.DataAccess.Models;
using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Managers
{
    public class GenericManager<TModel> : BaseManager<TModel, int>
		where TModel : class, IModelWithNumericKey
    {
		public GenericManager( Workspace workspace ) : base( workspace ) { }
		public GenericManager( DbContext dataContext ) : base( dataContext ) { }

		protected override Expression<Func<TModel, int>> KeyExpression => model => model.Id;

		// Expose as public
		public new IQueryable<TModel> Set => base.Set;
	}
}
