﻿namespace Arcanox.AspNetCoreHelpers.DataAccess.Models
{
    public interface IModelWithNumericKey
    {
        int Id { get; }
    }
}
