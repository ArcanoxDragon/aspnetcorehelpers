﻿using System;

namespace Arcanox.AspNetCoreHelpers.DataAccess
{
	public class RollbackException : Exception { }
}