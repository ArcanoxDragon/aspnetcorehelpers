﻿using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.DataAccess
{
	public class UnitOfWork
	{
		internal UnitOfWork( Workspace workspace, DbContext dataContext )
		{
			this.Workspace   = workspace;
			this.DataContext = dataContext;
		}

		public Workspace Workspace   { get; }
		public DbContext DataContext { get; }

		public void Rollback() => throw new RollbackException();
	}
}