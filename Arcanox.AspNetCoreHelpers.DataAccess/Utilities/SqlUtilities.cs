﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Arcanox.AspNetCoreHelpers.Extensions;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Utilities
{
	public static class SqlUtilities
	{
		// TODO:
		/*public static IQueryable<TSource> ExcludeColumns<TSource>( this IQueryable<TSource> source, params string[] columnsToExclude )
			where TSource : class
		{
			var sqlString = source.ToSql();

			foreach ( var column in columnsToExclude )
				sqlString = Regex.Replace( sqlString, $@"(SELECT|,)([^,]*`{column}`)", m => $"{m.Groups[ 1 ].Value} null AS `{column}`" );

			return source.FromSql( sqlString );
		}

		public static IQueryable<TSource> ExcludeColumns<TSource>( this IQueryable<TSource> source,
																   params Expression<Func<TSource, object>>[] columnExpressions )
			where TSource : class
		{
			var columnNames = columnExpressions.Select( ExpressionHelper.GetExpressionText ).ToArray();

			return source.ExcludeColumns( columnNames );
		}*/
	}
}