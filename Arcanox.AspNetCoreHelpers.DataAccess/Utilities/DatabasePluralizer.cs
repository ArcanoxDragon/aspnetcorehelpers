﻿using Humanizer;
using Microsoft.EntityFrameworkCore.Design;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Utilities
{
	public class DatabasePluralizer : IPluralizer
	{
		public string Pluralize( string identifier ) => identifier?.Pluralize( false );
		public string Singularize( string identifier ) => identifier?.Singularize( false );
	}
}
