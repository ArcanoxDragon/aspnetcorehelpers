﻿using System;
using Arcanox.AspNetCoreHelpers.Attributes;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Attributes
{
	[ AttributeUsage( AttributeTargets.Class, Inherited = false ) ]
	public class ManagerAttribute : ServiceAttribute { }
}