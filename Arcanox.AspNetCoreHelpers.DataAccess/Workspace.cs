﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.DataAccess.Extensions;
using Arcanox.AspNetCoreHelpers.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.DataAccess
{
	public class Workspace
	{
		public Workspace( DbContext dataContext )
		{
			this.DataContext = dataContext;
		}

		public DbContext DataContext { get; }

		public IQueryable<T> Set<T>() where T : class => this.DataContext.Set<T>();

		#region Unit of work

		#region Asynchronous

		public async Task UnitOfWorkAsync( Func<UnitOfWork, CancellationToken, Task> workFunction, bool useTransaction = false, CancellationToken token = default )
		{
			var unitOfWork = new UnitOfWork( this, this.DataContext );

			if ( useTransaction && this.DataContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = await this.DataContext.Database.BeginTransactionAsync( token ) )
				{
					try
					{
						await workFunction( unitOfWork, token );
						await this.DataContext.SaveChangesAsync( token );
						transaction.Commit();
					}
					catch ( RollbackException )
					{
						transaction.TryRollback();
					}
					catch
					{
						transaction.TryRollback();
						throw;
					}
				}
			}
			else
			{
				await workFunction( unitOfWork, token );
				await this.DataContext.SaveChangesAsync( token );
			}
		}

		public async Task<TResult> UnitOfWorkAsync<TResult>( Func<UnitOfWork, CancellationToken, Task<TResult>> workFunction, bool useTransaction = false, CancellationToken token = default )
		{
			var unitOfWork = new UnitOfWork( this, this.DataContext );

			if ( useTransaction && this.DataContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = await this.DataContext.Database.BeginTransactionAsync( token ) )
				{
					var ret = default( TResult );
					try
					{
						ret = await workFunction( unitOfWork, token );
						await this.DataContext.SaveChangesAsync( token );
						transaction.Commit();
						return ret;
					}
					catch ( RollbackException )
					{
						transaction.TryRollback();
						return ret;
					}
					catch
					{
						transaction.TryRollback();
						throw;
					}
				}
			}
			else
			{
				var ret = await workFunction( unitOfWork, token );
				await this.DataContext.SaveChangesAsync( token );
				return ret;
			}
		}

		#endregion

		#region Synchronous

		public void UnitOfWork( Action<UnitOfWork> workFunction, bool useTransaction = false )
		{
			var unitOfWork = new UnitOfWork( this, this.DataContext );

			if ( useTransaction && this.DataContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = this.DataContext.Database.BeginTransaction() )
				{
					try
					{
						workFunction( unitOfWork );
						this.DataContext.SaveChanges();
						transaction.Commit();
					}
					catch ( RollbackException )
					{
						transaction.TryRollback();
					}
					catch
					{
						transaction.TryRollback();
						throw;
					}
				}
			}
			else
			{
				workFunction( unitOfWork );
				this.DataContext.SaveChanges();
			}
		}

		public TResult UnitOfWork<TResult>( Func<UnitOfWork, TResult> workFunction, bool useTransaction = false )
		{
			var unitOfWork = new UnitOfWork( this, this.DataContext );

			if ( useTransaction && this.DataContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = this.DataContext.Database.BeginTransaction() )
				{
					var ret = default( TResult );
					try
					{
						ret = workFunction( unitOfWork );
						this.DataContext.SaveChanges();
						transaction.Commit();
						return ret;
					}
					catch ( RollbackException )
					{
						transaction.TryRollback();
						return ret;
					}
					catch
					{
						transaction.TryRollback();
						throw;
					}
				}
			}
			else
			{
				var ret = workFunction( unitOfWork );
				this.DataContext.SaveChanges();
				return ret;
			}
		}

		#endregion

		#endregion

		#region Make changes

		#region Asynchronous

		public async Task MakeChangesAsync<TEntity>( TEntity entity,
													 Func<UnitOfWork, TEntity, CancellationToken, Task> workFunction,
													 CancellationToken token = default ) where TEntity : class
		{
			entity.EnsureNotNull( nameof(entity) );

			var entityKey     = this.DataContext.GetKeyValue<object, TEntity>( entity );
			var trackedEntity = this.DataContext.Set<TEntity>().Find( entityKey );

			await this.UnitOfWorkAsync( async uow => {
				await workFunction( uow, trackedEntity, token );
				await this.DataContext.SaveChangesAsync( token );
			}, true, token );
		}

		#endregion

		#region Synchronous

		public void MakeChanges<TEntity>( TEntity entity, Action<UnitOfWork, TEntity> workFunction ) where TEntity : class
		{
			entity.EnsureNotNull( nameof(entity) );

			var entityKey     = this.DataContext.GetKeyValue<object, TEntity>( entity );
			var trackedEntity = this.DataContext.Set<TEntity>().Find( entityKey );

			this.UnitOfWork( uow => {
				workFunction( uow, trackedEntity );
				this.DataContext.SaveChanges();
			} );
		}

		#endregion

		#endregion
	}
}