﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Arcanox.AspNetCoreHelpers.Attributes;
using Arcanox.AspNetCoreHelpers.DataAccess.Abstract;
using Arcanox.AspNetCoreHelpers.DataAccess.Attributes;
using Arcanox.AspNetCoreHelpers.DataAccess.Managers;
using Arcanox.AspNetCoreHelpers.DataAccess.Models;
using Arcanox.AspNetCoreHelpers.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static IServiceCollection AddWorkspaces<TDataContext>( this IServiceCollection services )
			where TDataContext : DbContext
			=> services.AddScoped( provider => new Workspace( (DbContext) provider.GetRequiredService( typeof( TDataContext ) ) ) );

		public static void AddWorkspaces( this IServiceCollection services )
			=> services.AddWorkspaces<DbContext>();

		/// <summary>
		/// Finds all types annotated with a Manager attribute and registers
		/// them in the <paramref name="services"/> collection.
		/// </summary>
		/// <param name="services">The service collection to which the discovered services will be registered</param>
		/// <param name="assemblies">
		/// An array of assembly names in which to search for manager classes. If unspecified,
		/// all assemblies loaded in the current AppDomain will be searched.
		/// </param>
		public static IServiceCollection FindAndAddManagers( this IServiceCollection services, params string[] assemblies )
		{
			IEnumerable<Type> GetTypesWithAttributeSorted<TAttribute>() where TAttribute : ServiceAttribute
			{
				var typesWithAttribute = ReflectionHelper.GetTypesWithAttribute<TAttribute>( assemblies );

				return typesWithAttribute.OrderByDescending( type => type.GetCustomAttribute<TAttribute>().Priority );
			}

			foreach ( var type in GetTypesWithAttributeSorted<ManagerAttribute>() )
			{
				var syncInterface  = type.GetInterfaces().SingleOrDefault( iface => iface.GetGenericTypeDefinition() == typeof( IManager<,> ) );
				var asyncInterface = type.GetInterfaces().SingleOrDefault( iface => iface.GetGenericTypeDefinition() == typeof( IAsyncManager<,> ) );

				if ( syncInterface != null )
					services.TryAddTransient( syncInterface, type );

				if ( asyncInterface != null )
					services.TryAddTransient( asyncInterface, type );

				services.TryAddTransient( type );
			}

			// Automatically add generic managers for models implementing IModelWithNumericIndex
			foreach ( var type in ReflectionHelper.GetTypesWithAttribute<ModelAttribute>( false, assemblies ) )
			{
				if ( typeof( IModelWithNumericKey ).IsAssignableFrom( type ) )
				{
					var managerType = typeof( GenericManager<> ).MakeGenericType( type );

					services.TryAddTransient( managerType );
				}
			}

			return services;
		}
	}
}