﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Extensions
{
	public static class WorkspaceUnitOfWorkExtensions
	{
		public static Task UnitOfWorkAsync( this Workspace workspace,
											Func<UnitOfWork, Task> workFunction,
											bool useTransaction = false,
											CancellationToken token = default )
			=> workspace.UnitOfWorkAsync( ( uow, t ) => workFunction( uow ), useTransaction, token );

		public static Task<T> UnitOfWorkAsync<T>( this Workspace workspace,
												  Func<UnitOfWork, Task<T>> workFunction,
												  bool useTransaction = false,
												  CancellationToken token = default )
			=> workspace.UnitOfWorkAsync( ( uow, t ) => workFunction( uow ), useTransaction, token );

		public static Task UnitOfWorkAsync( this Workspace workspace,
											Action<UnitOfWork> workFunction,
											bool useTransaction = false,
											CancellationToken token = default )
			=> workspace.UnitOfWorkAsync( uow => {
				workFunction( uow );
				return Task.CompletedTask;
			}, useTransaction, token );

		public static Task<T> UnitOfWorkAsync<T>( this Workspace workspace,
												  Func<UnitOfWork, T> workFunction,
												  bool useTransaction = false,
												  CancellationToken token = default )
			=> workspace.UnitOfWorkAsync( uow => Task.FromResult( workFunction( uow ) ), useTransaction, token );
	}
}