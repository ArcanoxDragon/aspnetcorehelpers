﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Extensions
{
	public static class WorkspaceMakeChangesExtensions
	{
		public static Task MakeChangesAsync<TEntity>( this Workspace workspace,
													  TEntity obj,
													  Func<UnitOfWork, TEntity, Task> workFunction,
													  CancellationToken token = default )
			where TEntity : class
			=> workspace.MakeChangesAsync( obj, ( uow, o, t ) => workFunction( uow, o ), token );

		public static Task MakeChangesAsync<TEntity>( this Workspace workspace,
													  TEntity obj,
													  Action<UnitOfWork, TEntity> workFunction,
													  CancellationToken token = default )
			where TEntity : class
			=> workspace.MakeChangesAsync( obj, ( uow, o ) => {
				workFunction( uow, o );
				return Task.CompletedTask;
			}, token );

		public static Task MakeChangesAsync<TEntity>( this Workspace workspace,
													  TEntity obj,
													  Func<TEntity, Task> workFunction,
													  CancellationToken token = default )
			where TEntity : class
			=> workspace.MakeChangesAsync( obj, ( uow, o ) => workFunction( o ), token );

		public static Task MakeChangesAsync<TEntity>( this Workspace workspace,
													  TEntity obj,
													  Action<TEntity> workFunction,
													  CancellationToken token = default )
			where TEntity : class
			=> workspace.MakeChangesAsync( obj, ( uow, o ) => {
				workFunction( o );
				return Task.CompletedTask;
			}, token );

		public static void MakeChanges<TEntity>( this Workspace workspace,
												 TEntity obj,
												 Action<TEntity> workFunction )
			where TEntity : class
			=> workspace.MakeChanges( obj, ( uow, o ) => workFunction( o ) );
	}
}