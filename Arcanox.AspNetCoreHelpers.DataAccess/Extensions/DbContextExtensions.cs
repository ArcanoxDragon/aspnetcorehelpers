﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Extensions
{
	public static class DbContextExtensions
	{
		public static TKey GetKeyValue<TKey, TEntity>( this DbContext dbContext, TEntity entity )
		{
			entity.EnsureNotNull( nameof(entity) );

			var entityType = dbContext.Model.FindEntityType( typeof( TEntity ) );
			var primaryKey = entityType.FindPrimaryKey();

			if ( primaryKey == null )
				throw new InvalidOperationException( $"Entity \"{typeof( TEntity ).Name}\" does not have a primary key defined" );

			if ( primaryKey.Properties.Count != 1 )
				throw new NotSupportedException( "Only single-property keys are supported" );

			var property = primaryKey.Properties.Single();

			if ( !typeof( TKey ).IsAssignableFrom( property.ClrType ) )
				throw new InvalidOperationException( $"Inconsistent key type for entity \"{typeof( TEntity ).Name}\": " +
													 $"key is defined as \"{property.ClrType.Name}\" but was requested as \"{typeof( TKey ).Name}\"" );

			if ( property.PropertyInfo != null )
				return (TKey) property.PropertyInfo.GetValue( entity );

			if ( property.FieldInfo != null )
				return (TKey) property.FieldInfo.GetValue( entity );

			throw new InvalidOperationException( $"Key for entity \"{typeof( TEntity ).Name}\" does not have a property or field defined" );
		}

		public static void SkipLazyLoading( this DbContext context, Action withoutLazyLoading )
		{
			var previousLazyLoading = context.ChangeTracker.LazyLoadingEnabled;

			context.ChangeTracker.LazyLoadingEnabled = false;
			withoutLazyLoading();
			context.ChangeTracker.LazyLoadingEnabled = previousLazyLoading;
		}

		public static async Task SkipLazyLoadingAsync( this DbContext context, Func<Task> withoutLazyLoading )
		{
			var previousLazyLoading = context.ChangeTracker.LazyLoadingEnabled;

			context.ChangeTracker.LazyLoadingEnabled = false;
			await withoutLazyLoading();
			context.ChangeTracker.LazyLoadingEnabled = previousLazyLoading;
		}

		public static TResult SkipLazyLoading<TResult>( this DbContext context, Func<TResult> withoutLazyLoading )
		{
			var     previousLazyLoading = context.ChangeTracker.LazyLoadingEnabled;
			TResult result;

			context.ChangeTracker.LazyLoadingEnabled = false;
			result                                   = withoutLazyLoading();
			context.ChangeTracker.LazyLoadingEnabled = previousLazyLoading;

			return result;
		}

		public static async Task<TResult> SkipLazyLoadingAsync<TResult>( this DbContext context, Func<Task<TResult>> withoutLazyLoading )
		{
			var     previousLazyLoading = context.ChangeTracker.LazyLoadingEnabled;
			TResult result;

			context.ChangeTracker.LazyLoadingEnabled = false;
			result                                   = await withoutLazyLoading();
			context.ChangeTracker.LazyLoadingEnabled = previousLazyLoading;

			return result;
		}
	}
}