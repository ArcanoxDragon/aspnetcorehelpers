﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Extensions
{
	/// <inheritdoc />
	/// <summary>
	/// Thrown to roll back a database transaction without the exception bubbling out of the UnitOfWork function
	/// </summary>
	public class RollbackException : Exception { }

	public static class EntityFrameworkExtensions
	{
		public static EntityEntry<TEntity> AttachIfNeeded<TEntity>( this DbContext dbContext, TEntity obj ) where TEntity : class
		{
			var entity = dbContext.Set<TEntity>().Local.FirstOrDefault( e => e == obj );
			var entry  = default( EntityEntry<TEntity> );

			if ( entity == null )
			{
				try
				{
					return dbContext.Attach( obj );
				}
				catch ( InvalidOperationException )
				{
					// Some stupid piece of code left something hanging and we have to go detach it
					entry = dbContext.Entry( obj );
					entry.DetachAll();
					return dbContext.Attach( obj );
				}
			}

			entry = dbContext.Entry( obj );

			if ( entry.State == EntityState.Detached )
				entry.State = EntityState.Unchanged;

			return entry;
		}

		public static void DetachAll( this EntityEntry entry )
		{
			entry.State = EntityState.Detached;

			foreach ( var reference in entry.References.Where( reference => reference.TargetEntry?.State != EntityState.Detached ) )
				reference.TargetEntry?.DetachAll();
		}

		/// <summary>
		/// Starts making changes to the given <typeparamref name="TEntity"/>.
		/// Changes made to the entity inside the <paramref name="workFunction"/> will be saved
		/// to the database if the function successfully returns.
		/// 
		/// If an error occurs in the <paramref name="workFunction"/>, the entity
		/// will be reverted to the current database state.
		/// 
		/// The entity will stop being tracked once this function exits, no matter what.
		/// </summary>
		/// <typeparam name="TEntity">The type of entity to make changes to</typeparam>
		/// <param name="dbContext">The context in which the entity will be tracked</param>
		/// <param name="entity">The entity object to which the changes will be made</param>
		/// <param name="workFunction">The function inside which the changes will be made</param>
		public static void MakeChanges<TEntity>( this DbContext dbContext, TEntity entity, Action<TEntity> workFunction )
			where TEntity : class
		{
			entity.EnsureNotNull( nameof(entity) );

			var entityKey     = dbContext.GetKeyValue<object, TEntity>( entity );
			var trackedEntity = dbContext.Set<TEntity>().Find( entityKey );

			try
			{
				dbContext.UnitOfWork( () => {
					workFunction( trackedEntity );
					dbContext.SaveChanges();
				} );
			}
			finally
			{
				dbContext.Entry( trackedEntity ).DetachAll();
			}
		}

		/// <summary>
		/// Starts asynchronously making changes to the given <typeparamref name="TEntity"/>.
		/// Changes made to the entity inside the <paramref name="workFunction"/> will be saved
		/// to the database if the function successfully returns.
		/// 
		/// If an error occurs in the <paramref name="workFunction"/>, the entity
		/// will be reverted to the current database state.
		/// 
		/// The entity will stop being tracked once this function exits, no matter what.
		/// </summary>
		/// <typeparam name="TEntity">The type of entity to make changes to</typeparam>
		/// <param name="dbContext">The context in which the entity will be tracked</param>
		/// <param name="entity">The entity object to which the changes will be made</param>
		/// <param name="workFunction">The function inside which the changes will be made</param>
		/// <param name="token">A cancellation token to use during asynchronous operations</param>
		public static async Task MakeChangesAsync<TEntity>( this DbContext dbContext, TEntity entity, Func<TEntity, CancellationToken, Task> workFunction, CancellationToken token = default )
			where TEntity : class
		{
			entity.EnsureNotNull( nameof(entity) );

			var entityKey     = dbContext.GetKeyValue<object, TEntity>( entity );
			var trackedEntity = dbContext.Set<TEntity>().Find( entityKey );

			try
			{
				await dbContext.UnitOfWorkAsync( async () => {
					await workFunction( trackedEntity, token );
					await dbContext.SaveChangesAsync( token );
				}, token );
			}
			finally
			{
				dbContext.Entry( trackedEntity ).DetachAll();
			}
		}

		/// <inheritdoc cref="MakeChangesAsync{TEntity}(DbContext,TEntity,Func{TEntity,CancellationToken,Task},CancellationToken)"/>
		public static Task MakeChangesAsync<TEntity>( this DbContext dbContext, TEntity obj, Func<TEntity, Task> workFunction, CancellationToken token = default )
			where TEntity : class
			=> dbContext.MakeChangesAsync( obj, ( o, t ) => workFunction( o ), token );

		/// <inheritdoc cref="MakeChangesAsync{TEntity}(DbContext,TEntity,Func{TEntity,CancellationToken,Task},CancellationToken)"/>
		public static Task MakeChangesAsync<TEntity>( this DbContext dbContext, TEntity obj, Action<TEntity> workFunction, CancellationToken token = default )
			where TEntity : class
			=> dbContext.MakeChangesAsync( obj, o => {
				workFunction( o );
				return Task.CompletedTask;
			}, token );

		/// <summary>
		/// Starts asynchronously making changes to the given <typeparamref name="TEntity"/>.
		/// Changes made to the entity inside the <paramref name="workFunction"/> will be saved
		/// to the database if the function successfully returns.
		/// 
		/// If an error occurs in the <paramref name="workFunction"/>, the entity
		/// will be reverted to the current database state.
		/// 
		/// The entity will stop being tracked once this function exits, no matter what.
		/// </summary>
		/// <typeparam name="TEntity">The type of entity to make changes to</typeparam>
		/// <typeparam name="TResult"></typeparam>
		/// <param name="dbContext">The context in which the entity will be tracked</param>
		/// <param name="entity">The entity object to which the changes will be made</param>
		/// <param name="workFunction">The function inside which the changes will be made</param>
		/// <param name="token">A cancellation token to use during asynchronous operations</param>
		public static async Task<TResult> MakeChangesAsync<TEntity, TResult>( this DbContext dbContext, TEntity entity, Func<TEntity, CancellationToken, Task<TResult>> workFunction, CancellationToken token = default )
			where TEntity : class
		{
			entity.EnsureNotNull( nameof(entity) );

			var entityKey     = dbContext.GetKeyValue<object, TEntity>( entity );
			var trackedEntity = dbContext.Set<TEntity>().Find( entityKey );

			try
			{
				return await dbContext.UnitOfWorkAsync( async () => {
					var result = await workFunction( trackedEntity, token );
					await dbContext.SaveChangesAsync( token );
					return result;
				}, token );
			}
			finally
			{
				dbContext.Entry( trackedEntity ).DetachAll();
			}
		}

		/// <inheritdoc cref="MakeChangesAsync{TEntity}(DbContext,TEntity,Func{TEntity,CancellationToken,Task},CancellationToken)"/>
		public static Task<TResult> MakeChangesAsync<TEntity, TResult>( this DbContext dbContext, TEntity obj, Func<TEntity, Task<TResult>> workFunction, CancellationToken token = default )
			where TEntity : class
			=> dbContext.MakeChangesAsync( obj, ( o, t ) => workFunction( o ), token );

		/// <inheritdoc cref="MakeChangesAsync{TEntity}(DbContext,TEntity,Func{TEntity,CancellationToken,Task},CancellationToken)"/>
		public static Task<TResult> MakeChangesAsync<TEntity, TResult>( this DbContext dbContext, TEntity obj, Func<TEntity, TResult> workFunction, CancellationToken token = default )
			where TEntity : class
			=> dbContext.MakeChangesAsync( obj, o => Task.FromResult( workFunction( o ) ), token );

		public static TReference ReadReference<TEntity, TReference>( this DbContext dbContext, TEntity obj, Expression<Func<TEntity, TReference>> reference, bool attach = false )
			where TEntity : class
			where TReference : class
		{
			var entry    = dbContext.AttachIfNeeded( obj );
			var refEntry = entry.Reference( reference );
			refEntry.Load();

			if ( !attach )
				entry.DetachAll();

			return refEntry.CurrentValue;
		}

		public static async Task<TReference> ReadReferenceAsync<TEntity, TReference>( this DbContext dbContext, TEntity obj, Expression<Func<TEntity, TReference>> reference, bool attach = false, CancellationToken token = default )
			where TEntity : class
			where TReference : class
		{
			var entry    = dbContext.AttachIfNeeded( obj );
			var refEntry = entry.Reference( reference );
			await refEntry.LoadAsync( token );

			if ( !attach )
				entry.DetachAll();

			return refEntry.CurrentValue;
		}

		public static CollectionEntry<TEntity, TCollection> ReadCollection<TEntity, TCollection>( this DbContext dbContext, TEntity obj, Expression<Func<TEntity, IEnumerable<TCollection>>> collection, bool attach = false )
			where TEntity : class
			where TCollection : class
		{
			var entry    = dbContext.AttachIfNeeded( obj );
			var refEntry = entry.Collection( collection );
			refEntry.Load();

			if ( !attach )
				entry.DetachAll();

			return refEntry;
		}

		public static async Task<CollectionEntry<TEntity, TCollection>> ReadCollectionAsync<TEntity, TCollection>( this DbContext dbContext, TEntity obj, Expression<Func<TEntity, IEnumerable<TCollection>>> collection, bool attach = false, CancellationToken token = default )
			where TEntity : class
			where TCollection : class
		{
			var entry    = dbContext.AttachIfNeeded( obj );
			var refEntry = entry.Collection( collection );
			await refEntry.LoadAsync( token );

			if ( !attach )
				entry.DetachAll();

			return refEntry;
		}

		public static async Task UnitOfWorkAsync( this DbContext dbContext, Func<CancellationToken, Task> workFunction, CancellationToken token = default )
		{
			if ( dbContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = await dbContext.Database.BeginTransactionAsync( token ) )
				{
					try
					{
						await workFunction( token );
						await dbContext.SaveChangesAsync( token );
						transaction.Commit();
					}
					catch ( RollbackException )
					{
						transaction.Rollback();
					}
					catch
					{
						transaction.Rollback();
						throw;
					}
				}
			}
			else
			{
				await workFunction( token );
				await dbContext.SaveChangesAsync( token );
			}
		}

		public static async Task<T> UnitOfWorkAsync<T>( this DbContext dbContext, Func<CancellationToken, Task<T>> workFunction, CancellationToken token = default )
		{
			if ( dbContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = await dbContext.Database.BeginTransactionAsync( token ) )
				{
					var ret = default( T );
					try
					{
						ret = await workFunction( token );
						await dbContext.SaveChangesAsync( token );
						transaction.Commit();
						return ret;
					}
					catch ( RollbackException )
					{
						transaction.Rollback();
						return ret;
					}
					catch
					{
						try
						{
							transaction.Rollback();
						}
						catch
						{
							/* Ignored */
						}

						throw;
					}
				}
			}
			else
			{
				var ret = await workFunction( token );
				await dbContext.SaveChangesAsync( token );
				return ret;
			}
		}

		public static Task UnitOfWorkAsync( this DbContext dbContext, Func<Task> workFunction, CancellationToken token = default )
			=> dbContext.UnitOfWorkAsync( t => workFunction(), token );

		public static Task<T> UnitOfWorkAsync<T>( this DbContext dbContext, Func<Task<T>> workFunction, CancellationToken token = default )
			=> dbContext.UnitOfWorkAsync( t => workFunction(), token );

		public static Task UnitOfWorkAsync( this DbContext dbContext, Action workFunction, CancellationToken token = default )
			=> dbContext.UnitOfWorkAsync( () => {
				workFunction();
				return Task.CompletedTask;
			}, token );

		public static Task<T> UnitOfWorkAsync<T>( this DbContext dbContext, Func<T> workFunction, CancellationToken token = default )
			=> dbContext.UnitOfWorkAsync( () => Task.FromResult( workFunction() ), token );

		public static void UnitOfWork( this DbContext dbContext, Action workFunction )
		{
			if ( dbContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = dbContext.Database.BeginTransaction() )
				{
					try
					{
						workFunction();
						dbContext.SaveChanges();
						transaction.Commit();
					}
					catch ( RollbackException )
					{
						transaction.Rollback();
					}
					catch
					{
						transaction.Rollback();
						throw;
					}
				}
			}
			else
			{
				workFunction();
				dbContext.SaveChanges();
			}
		}

		public static T UnitOfWork<T>( this DbContext dbContext, Func<T> workFunction )
		{
			if ( dbContext.Database.CurrentTransaction == null )
			{
				using ( var transaction = dbContext.Database.BeginTransaction() )
				{
					var ret = default( T );
					try
					{
						ret = workFunction();
						dbContext.SaveChanges();
						transaction.Commit();
						return ret;
					}
					catch ( RollbackException )
					{
						transaction.Rollback();
						return ret;
					}
					catch
					{
						transaction.Rollback();
						throw;
					}
				}
			}
			else
			{
				var ret = workFunction();
				dbContext.SaveChanges();
				return ret;
			}
		}
	}
}