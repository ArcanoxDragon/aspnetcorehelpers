﻿using Microsoft.EntityFrameworkCore.Storage;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Extensions
{
    static class TransactionExtensions
    {
		public static void TryRollback( this IDbContextTransaction transaction )
		{
			try
			{
				transaction.Rollback();
			}
			catch
			{
				// Ignored
			}
		}
    }
}
