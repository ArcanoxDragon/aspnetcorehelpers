﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Arcanox.AspNetCoreHelpers.Attributes;
using Arcanox.AspNetCoreHelpers.DataAccess.Abstract;
using Arcanox.AspNetCoreHelpers.DataAccess.Utilities;
using Arcanox.AspNetCoreHelpers.Utilities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Arcanox.AspNetCoreHelpers.DataAccess.Extensions
{
	public static class ModelBuilderExtensions
	{
		public const string RegisterModelMethodName        = "Register";
		public const string RegisterModelMethodName_Legacy = "BuildModelEntity";

		/// <summary>
		/// Finds all types annotated with a Model attribute and registers
		/// them as entities in the <paramref name="builder"/>
		/// </summary>
		/// <param name="builder">The <see cref="ModelBuilder"/> in which to register the entities</param>
		/// <param name="assemblies">
		/// An array of assembly names in which to search for translator classes. If unspecified,
		/// all assemblies loaded in the current AppDomain will be searched.
		/// </param>
		public static void FindAndAddEntities( this ModelBuilder builder, params string[] assemblies )
		{
			var entityMethod = builder.GetType()
									  .GetMethods( BindingFlags.Public | BindingFlags.Instance )
									  .FirstOrDefault( m => m.Name == "Entity" && m.IsGenericMethod );

			if ( entityMethod == null )
				throw new InvalidOperationException( "Could not find Entity<TEntity>() method on ModelBuilder" );

			foreach ( var type in ReflectionHelper.GetTypesWithAttribute<ModelAttribute>( false, assemblies ) )
			{
				try
				{
					builder.Entity( type );

					var method = type.GetMethod( RegisterModelMethodName, BindingFlags.Public | BindingFlags.Static );

					if ( method == null )
					{
						method = type.GetMethod( RegisterModelMethodName_Legacy, BindingFlags.Public | BindingFlags.Static );

						if ( method != null )
						{
							Debug.WriteLine( $"Warning: The model \"{type.FullName}\" uses the old {RegisterModelMethodName_Legacy} model builder method. " +
											 $"It should switch to the new {RegisterModelMethodName} method." );
						}
					}

					if ( method != null )
					{
						var typedEntityMethod = entityMethod.MakeGenericMethod( type );
						var entity            = typedEntityMethod.Invoke( builder, new object[] { } );

						method.Invoke( null, new[] { entity } );
					}
				}
				catch ( Exception ex )
				{
					// Fail-safe; ignore errors

					Debug.WriteLine( $"Error building model for type {type.FullName}:\n\n" );
					Debug.WriteLine( ex );
				}
			}
		}

		/// <summary>
		/// Finds all types annotated with a Translates attribute and registers
		/// them as <see cref="IDbFunctionTranslator"/>s in the <paramref name="builder"/>
		/// </summary>
		/// <param name="builder">The <see cref="ModelBuilder"/> in which to register the translators</param>
		/// <param name="assemblies">
		/// An array of assembly names in which to search for translator classes. If unspecified,
		/// all assemblies loaded in the current AppDomain will be searched.
		/// </param>
		public static void FindAndAddTranslators( this ModelBuilder builder, params string[] assemblies )
		{
			foreach ( var type in ReflectionHelper.GetTypesWithAttribute<TranslatesAttribute>( false, assemblies ) )
			{
				if ( !typeof( IDbFunctionTranslator ).IsAssignableFrom( type ) )
				{
					Debug.WriteLine( $"Error: Translator {type.Name} does not implement {nameof(IDbFunctionTranslator)}. " +
									 $"This translator will not be registered." );
					continue;
				}

				var attribute = type.GetCustomAttribute<TranslatesAttribute>();
				var method    = attribute.DeclaringType.GetMethod( attribute.MethodName, BindingFlags.Public | BindingFlags.Static );

				if ( method == null )
				{
					Debug.WriteLine( $"Error: Could not find method \"{attribute.MethodName}\" on type \"{attribute.DeclaringType.FullName}\".\n\n" );
					continue;
				}

				try
				{
					var singleton = (IDbFunctionTranslator) Activator.CreateInstance( type );

					builder.HasDbFunction( method, f => f.HasTranslation( singleton.Translate ) );
				}
				catch ( Exception ex )
				{
					// Fail-safe; ignore errors

					Debug.WriteLine( $"Error constructing translator of type {type.FullName}:\n\n" );
					Debug.WriteLine( ex );
				}
			}
		}

		public static void PluralizeTableNames( this ModelBuilder builder, IPluralizer pluralizer )
		{
			foreach ( var entity in builder.Model.GetEntityTypes() )
			{
				var tableName = entity.GetTableName();

				entity.SetTableName( pluralizer.Pluralize( tableName ) );
			}
		}

		public static void PluralizeTableNames( this ModelBuilder builder ) => builder.PluralizeTableNames( new DatabasePluralizer() );
	}
}