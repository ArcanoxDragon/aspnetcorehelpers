﻿using System;
using System.Linq.Expressions;
using NUnit.Framework;
using Arcanox.AspNetCoreHelpers.Utilities.Linq;
using MvcExpressionHelper = Microsoft.AspNetCore.Mvc.ViewFeatures.Internal.ExpressionHelper;

namespace Arcanox.AspNetCoreHelpers.Tests.Utilities.Linq
{
	[ TestFixture ]
	public class ExpressionHelperTests
	{
		private class TestModelOne
		{
			public string Apple  { get; set; }
			public int    Orange { get; set; }
		}

		private class TestModelTwo
		{
			public TestModelOne One    { get; set; }
			public string       Banana { get; set; }
		}

		private static void AssertExpressions( LambdaExpression expected, LambdaExpression actual )
		{
			var expectedText = expected.ToString();
			var actualText   = actual.ToString();

			Assert.AreEqual( expectedText, actualText );
		}

		[ Test ]
		public void TestModelOneExpression1()
		{
			Expression<Func<TestModelOne, string>> expected = m => m.Apple;
			LambdaExpression                       actual   = ExpressionHelper.GetExpression<TestModelOne>( "Apple" );

			AssertExpressions( expected, actual );
		}

		[ Test ]
		public void TestModelOneExpression2()
		{
			Expression<Func<TestModelOne, int>> expected = m => m.Orange;
			LambdaExpression                    actual   = ExpressionHelper.GetExpression<TestModelOne>( "Orange" );

			AssertExpressions( expected, actual );
		}

		[ Test ]
		public void TestModelTwoExpressions()
		{
			Expression<Func<TestModelTwo, object>> expected = m => m.One.Apple;
			LambdaExpression                       actual   = ExpressionHelper.GetExpression<TestModelTwo>( "One.Apple" );

			AssertExpressions( expected, actual );

			expected = m => m.Banana;
			actual   = ExpressionHelper.GetExpression<TestModelTwo>( "Banana" );

			AssertExpressions( expected, actual );
		}

		[ Test ]
		public void TestInvalidPropertyNames()
		{
			Assert.Throws<InvalidOperationException>( () => ExpressionHelper.GetExpression<TestModelOne>( "Banana" ) );
			Assert.Throws<InvalidOperationException>( () => ExpressionHelper.GetExpression<TestModelTwo>( "Two" ) );
			Assert.Throws<InvalidOperationException>( () => ExpressionHelper.GetExpression<TestModelOne>( "One.Pear" ) );
		}
	}
}