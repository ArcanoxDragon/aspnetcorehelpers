﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.EntityFrameworkCore.Storage;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class QueryableExtensions
	{
		public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>( this IQueryable<TSource> queryable,
																		 Expression<Func<TSource, TKey>> keySelector,
																		 bool descending )
		{
			if ( descending )
				return queryable.OrderByDescending( keySelector );

			return queryable.OrderBy( keySelector );
		}

		public static IQueryable<TSource> OrderByExpression<TSource>( this IQueryable<TSource> queryable, LambdaExpression sortExpression )
		{
			var callExpression = Expression.Call( typeof( Queryable ),
												  nameof(Queryable.OrderBy),
												  new[] { typeof( TSource ), sortExpression.ReturnType },
												  queryable.Expression,
												  Expression.Quote( sortExpression ) );

			return queryable.Provider.CreateQuery<TSource>( callExpression );
		}

		public static IQueryable<TSource> OrderByExpressionDescending<TSource>( this IQueryable<TSource> queryable, LambdaExpression sortExpression )
		{
			var callExpression = Expression.Call( typeof( Queryable ),
												  nameof(Queryable.OrderByDescending),
												  new[] { typeof( TSource ), sortExpression.ReturnType },
												  queryable.Expression,
												  Expression.Quote( sortExpression ) );

			return queryable.Provider.CreateQuery<TSource>( callExpression );
		}

		public static IQueryable<TSource> OrderByExpression<TSource>( this IQueryable<TSource> queryable, LambdaExpression sortExpression, bool descending )
		{
			if ( descending )
				return queryable.OrderByExpressionDescending( sortExpression );

			return queryable.OrderByExpression( sortExpression );
		}

		/*private static readonly TypeInfo QueryCompilerTypeInfo = typeof( QueryCompiler ).GetTypeInfo();

		private static readonly TypeInfo QueryModelGeneratorTypeInfo = typeof( QueryModelGenerator ).GetTypeInfo();

		private static readonly FieldInfo QueryCompilerField = typeof( EntityQueryProvider ).GetTypeInfo().DeclaredFields.First( x => x.Name == "_queryCompiler" );

		private static readonly FieldInfo QueryModelGeneratorField = QueryCompilerTypeInfo.DeclaredFields.First( x => x.Name == "_queryModelGenerator" );

		private static readonly FieldInfo NodeTypeProviderField = QueryModelGeneratorTypeInfo.DeclaredFields.First( x => x.Name == "_nodeTypeProvider" );

		private static readonly MethodInfo CreateQueryParserMethod = QueryModelGeneratorTypeInfo.DeclaredMethods.First( x => x.Name == "CreateQueryParser" );

		private static readonly FieldInfo DataBaseField = QueryCompilerTypeInfo.DeclaredFields.Single( x => x.Name == "_database" );

		private static readonly PropertyInfo DatabaseDependenciesField
			= typeof( Database ).GetTypeInfo().DeclaredProperties.Single( x => x.Name == "Dependencies" );

		/// <summary>
		/// Created from aspnet/EntityFrameworkCore#6482
		/// </summary>
		public static string ToSql<TEntity>( this IQueryable<TEntity> query ) where TEntity : class
		{
			if ( !( query is EntityQueryable<TEntity> ) && !( query is InternalDbSet<TEntity> ) )
				throw new ArgumentException( "Invalid query" );

			var queryCompiler                  = (IQueryCompiler) QueryCompilerField.GetValue( query.Provider );
			var queryModelGenerator            = (IQueryModelGenerator) QueryModelGeneratorField.GetValue( queryCompiler );
			var nodeTypeProvider               = (INodeTypeProvider) NodeTypeProviderField.GetValue( queryModelGenerator );
			var parser                         = (IQueryParser) CreateQueryParserMethod.Invoke( queryModelGenerator, new object[] { nodeTypeProvider } );
			var queryModel                     = parser.GetParsedQuery( query.Expression );
			var database                       = DataBaseField.GetValue( queryCompiler );
			var queryCompilationContextFactory = ( (DatabaseDependencies) DatabaseDependenciesField.GetValue( database ) ).QueryCompilationContextFactory;
			var queryCompilationContext        = queryCompilationContextFactory.Create( false );
			var modelVisitor                   = (RelationalQueryModelVisitor) queryCompilationContext.CreateQueryModelVisitor();
			modelVisitor.CreateQueryExecutor<TEntity>( queryModel );
			var sql = modelVisitor.Queries.First().ToString();

			return sql;
		}*/
	}
}