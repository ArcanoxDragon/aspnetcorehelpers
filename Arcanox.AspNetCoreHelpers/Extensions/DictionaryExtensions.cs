﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class DictionaryExtensions
	{
		public static string ToElementAttributes<TKey, TValue>( this IDictionary<TKey, TValue> dictionary, string attributePrefix = "" )
			=> dictionary.Select( p => $@"{attributePrefix}{p.Key}=""{p.Value.ToString().HtmlEncode()}""" ).Join( " " );

		public static IEnumerable<(TKey key, TValue)> Pairs<TKey, TValue>( this IDictionary<TKey, TValue> dictionary )
			=> ( (IReadOnlyDictionary<TKey, TValue>) dictionary ).Pairs();

		public static IEnumerable<(TKey key, TValue)> Pairs<TKey, TValue>( this IReadOnlyDictionary<TKey, TValue> dictionary )
		{
			foreach ( var key in dictionary.Keys )
			{
				yield return ( key, dictionary[ key ] );
			}
		}

		public static IDictionary<TKey, TValue> MergeWith<TKey, TValue>( this IDictionary<TKey, TValue> dictionary, IDictionary<TKey, TValue> other )
		{
			var newDict = new Dictionary<TKey, TValue>();

			foreach ( var (key, value) in dictionary.Pairs() )
				newDict[ key ] = value;

			foreach ( var (key, value) in other.Pairs() )
				newDict[ key ] = value;

			return newDict;
		}

		public static void Deconstruct<TKey, TValue>( this KeyValuePair<TKey, TValue> pair, out TKey key, out TValue value )
		{
			key   = pair.Key;
			value = pair.Value;
		}

		public static object ToObject<TValue>( this IDictionary<string, TValue> dictionary )
		{
			var expando = new ExpandoObject();
			var dict    = (IDictionary<string, object>) expando;

			foreach ( var (key, value) in dictionary )
			{
				dict[ key ] = value;
			}

			return expando;
		}
	}
}