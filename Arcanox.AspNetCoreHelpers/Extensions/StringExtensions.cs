﻿using System;
using System.Text.Encodings.Web;
using JetBrains.Annotations;
using Microsoft.Extensions.Primitives;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class StringExtensions
	{
		[ ContractAnnotation( "s:null => true" ) ]
		public static bool IsNullOrEmpty( this string s ) // Extension alias
			=> string.IsNullOrEmpty( s );

		[ ContractAnnotation( "s:null => true" ) ]
		public static bool IsNullOrWhiteSpace( this string s ) // Extension alias
			=> string.IsNullOrWhiteSpace( s );

		public static bool IsNullOrEmpty( this StringValues s )
			=> s.ToString().IsNullOrEmpty();

		public static bool IsNullOrWhiteSpace( this StringValues s )
			=> s.ToString().IsNullOrWhiteSpace();

		public static bool EqualsIgnoreCase( this string s, string other )
			=> s.Equals( other, StringComparison.OrdinalIgnoreCase );

		public static bool ContainsIgnoreCase( this string s, string other )
			=> s.IndexOf( other, StringComparison.OrdinalIgnoreCase ) >= 0;

		public static string HtmlEncode( this string s )
			=> HtmlEncoder.Default.Encode( s );

		public static string Repeat( this string s, int count )
		{
			switch ( count )
			{
				case 0: return "";
				case 1: return s;
				default:
				{
					var r = "";

					for ( int i = 0; i < count; i++ )
						r += s;

					return r;
				}
			}
		}

		public static int? TryParseInt( this string s )
		{
			if ( int.TryParse( s, out var i ) )
				return i;

			return default;
		}
	}
}