﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
    public static class UrlHelperExtensions
    {
		public static string Controller( this IUrlHelper url, [AspMvcController] string controller, object values )
		{
			var actionUrl = url.Action( "", controller, values );

			return actionUrl;
		}

		public static string Controller( this IUrlHelper url, [ AspMvcController ] string controller )
			=> url.Controller( controller, null );

		public static string Controller( this IUrlHelper url )
			=> url.Controller( null, null );
	}
}
