﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class ObjectExtensions
	{
		public static IReadOnlyDictionary<string, string> ToDictionary( this object obj )
		{
			var dictionary = new Dictionary<string, string>();

			foreach ( var property in obj.GetType().GetProperties( BindingFlags.Public | BindingFlags.Instance )
										 .Where( prop => prop.CanRead ) )
			{
				var value = property.GetValue( obj );

				if ( value != null )
				{
					dictionary[ property.Name ] = value.ToString();
				}
			}

			return dictionary;
		}

		public static TTarget MapTo<TTarget>( this object source, TTarget target, params string[] excludeProperties )
		{
			target.EnsureNotNull( nameof(target) );

			var sourceType = source.GetType();
			var targetType = target.GetType();

			foreach ( var sourceProperty in sourceType.GetProperties( BindingFlags.Public | BindingFlags.Instance )
													  .Where( p => p.CanRead ) )
			{
				if ( sourceProperty.Name.In( excludeProperties ) )
					continue;

				var targetProperty = targetType.GetProperties( BindingFlags.Public | BindingFlags.Instance )
											   .SingleOrDefault( p => p.Name == sourceProperty.Name &&
																	  p.PropertyType == sourceProperty.PropertyType &&
																	  p.CanWrite );

				if ( targetProperty == null )
					continue;

				try
				{
					targetProperty.SetValue( target, sourceProperty.GetValue( source ) );
				}
				catch ( Exception ex )
				{
					// Ignore errors (fail silently when unable to set property)

					Debug.WriteLine( $"Exception mapping property {targetProperty.Name} from type {sourceType.FullName} to type {targetType.FullName}" );
					Debug.WriteLine( ex.ToString() );
				}
			}

			return target;
		}

		public static TTarget MapTo<TTarget>( this object source ) where TTarget : new()
		{
			var target = new TTarget();

			source.MapTo( target );

			return target;
		}

		public static T With<T>( this T obj, Action<T> transformer )
		{
			obj.EnsureNotNull( nameof(obj) );

			transformer( obj );

			return obj;
		}

		public static bool In<T>( this T obj, params T[] options )
		{
			obj.EnsureNotNull( nameof(obj) );

			return options.Any( o => o.Equals( obj ) );
		}
	}
}