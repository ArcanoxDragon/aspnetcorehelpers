﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class AttributeDictionaryExtensions
	{
		public static AttributeDictionary Without( this AttributeDictionary attributes, string name )
		{
			var newDict = new AttributeDictionary();

			foreach ( var pair in attributes.Where( p => !p.Key.Equals( name, StringComparison.OrdinalIgnoreCase ) ) )
				newDict.Add( pair );

			return newDict;
		}
	}
}