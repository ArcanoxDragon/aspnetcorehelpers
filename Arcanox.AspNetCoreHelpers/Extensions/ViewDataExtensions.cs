﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class ViewDataExtensions
	{
		public static void Put<T>( this ViewDataDictionary viewData, string key, T value )
		{
			if ( value is string )
				viewData[ key ] = value;
			else
				viewData[ key ] = JsonConvert.SerializeObject( value );
		}

		public static T Get<T>( this ViewDataDictionary viewData, string key )
		{
			viewData.TryGetValue( key, out var o );
			if ( typeof( T ) == typeof( string ) )
				return (T) o;
			return o == null ? default : JsonConvert.DeserializeObject<T>( (string) o );
		}

		public static ViewDataDictionary With( this ViewDataDictionary viewData, string key, object value )
		{
			return new ViewDataDictionary( viewData ) { { key, value } };
		}
	}
}