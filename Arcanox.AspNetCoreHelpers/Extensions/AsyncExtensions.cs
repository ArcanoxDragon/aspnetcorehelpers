﻿using System.Threading.Tasks;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
    public static class AsyncExtensions
    {
		/// <summary>
		/// Configures the task to continue without any captured context,
		/// and leaves the task to run without tracking it any longer.
		/// </summary>
		public static void Forget( this Task task ) => task.ConfigureAwait( false );
	}
}
