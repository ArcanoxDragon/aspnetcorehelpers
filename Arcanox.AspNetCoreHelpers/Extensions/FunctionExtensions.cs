﻿using System;
using JetBrains.Annotations;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class FunctionExtensions
	{
		[ NotNull ]
		public static T EnsureNotNull<T>( this T arg, string argName )
		{
			if ( object.Equals( arg, null ) )
				throw new ArgumentNullException( argName );

			return arg;
		}
	}
}