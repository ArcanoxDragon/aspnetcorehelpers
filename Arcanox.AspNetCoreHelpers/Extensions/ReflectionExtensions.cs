﻿using System;
using System.Collections.Generic;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
    public static class ReflectionExtensions
    {
		public static IEnumerable<Type> GetInterfacesRecursive( this Type type )
		{
			foreach ( var @interface in type.GetInterfaces() )
			{
				yield return @interface;

				foreach ( var parentInterface in @interface.GetInterfacesRecursive() )
					yield return parentInterface;
			}
		}
    }
}
