﻿using System;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	// TODO: Remove these once targeting netstandard2.1.
	// These implementations were taken from
	// https://github.com/dotnet/corefx/blob/master/src/Common/src/CoreLib/System/ArraySegment.cs#L147-L169
	public static class ArraySegmentExtensions
	{
		public static ArraySegment<T> Slice<T>( this ArraySegment<T> arraySegment, int index )
		{
			ThrowInvalidOperationIfDefault( arraySegment );

			if ( (uint) index > (uint) arraySegment.Count )
			{
				throw new ArgumentOutOfRangeException( nameof(index) );
			}

			return new ArraySegment<T>( arraySegment.Array, arraySegment.Offset + index, arraySegment.Count - index );
		}

		public static ArraySegment<T> Slice<T>( this ArraySegment<T> arraySegment, int index, int count )
		{
			ThrowInvalidOperationIfDefault( arraySegment );

			if ( (uint) index > (uint) arraySegment.Count || (uint) count > (uint) ( arraySegment.Count - index ) )
			{
				throw new ArgumentOutOfRangeException( nameof(index) );
			}

			return new ArraySegment<T>( arraySegment.Array, arraySegment.Offset + index, count );
		}

		private static void ThrowInvalidOperationIfDefault<T>( ArraySegment<T> arraySegment )
		{
			if ( arraySegment.Array == null )
			{
				throw new InvalidOperationException( "Array cannot be null" );
			}
		}
	}
}