﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class TagHelperAttributeListExtensions
	{
		[ Obsolete( "Renamed to AppendClasses" ) ]
		public static void AddClasses( this TagHelperAttributeList attributes, params string[] classes )
			=> attributes.AppendClasses( classes );

		public static void AppendClasses( this TagHelperAttributeList attributes, params string[] classes )
		{
			var existingClassesAttr = attributes[ "class" ]?.Value.ToString() ?? "";
			var existingClasses = existingClassesAttr.Split( ' ' )
													 .Where( c => !string.IsNullOrWhiteSpace( c ) );
			var classesToAdd = classes.Where( c => !existingClasses.Contains( c, StringComparer.OrdinalIgnoreCase ) );

			attributes.SetAttribute( "class", string.Join( " ", existingClasses.Concat( classesToAdd ) ) );
		}

		public static void PrependClasses( this TagHelperAttributeList attributes, params string[] classes )
		{
			var existingClassesAttr = attributes[ "class" ]?.Value.ToString() ?? "";
			var existingClasses = existingClassesAttr.Split( ' ' )
													 .Where( c => !string.IsNullOrWhiteSpace( c ) );
			var classesToAdd = classes.Where( c => !existingClasses.Contains( c, StringComparer.OrdinalIgnoreCase ) );

			attributes.SetAttribute( "class", string.Join( " ", classesToAdd.Concat( existingClasses ) ) );
		}
	}
}