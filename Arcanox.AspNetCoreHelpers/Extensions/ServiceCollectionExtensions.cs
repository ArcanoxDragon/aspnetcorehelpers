﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Arcanox.AspNetCoreHelpers.Attributes;
using Arcanox.AspNetCoreHelpers.Utilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class ServiceCollectionExtensions
	{
		/// <summary>
		/// Finds all types annotated with a service lifecycle attribute and registers
		/// them in the <paramref name="services"/> collection.
		/// </summary>
		/// <param name="services">The service collection to which the discovered services will be registered</param>
		/// <param name="assemblies">
		/// An array of assembly names in which to search for service classes. If unspecified,
		/// all assemblies loaded in the current AppDomain will be searched.
		/// </param>
		public static IServiceCollection FindAndAddServices( this IServiceCollection services, params string[] assemblies )
		{
			IEnumerable<Type> GetTypesWithAttributeSorted<TAttribute>() where TAttribute : ServiceAttribute
			{
				var typesWithAttribute = ReflectionHelper.GetTypesWithAttribute<TAttribute>( assemblies );

				return typesWithAttribute.OrderByDescending( type => type.GetCustomAttribute<TAttribute>().Priority );
			}

			// Reflect to automatically add our services
			foreach ( var type in GetTypesWithAttributeSorted<SingletonAttribute>() )
			{
				var attribute = type.GetCustomAttribute<SingletonAttribute>();

				if ( attribute.ContractType == null )
					services.TryAddSingleton( type );
				else
					services.TryAddSingleton( attribute.ContractType, type );
			}

			foreach ( var type in GetTypesWithAttributeSorted<ScopedAttribute>() )
			{
				var attribute = type.GetCustomAttribute<ScopedAttribute>();

				if ( attribute.ContractType == null )
					services.TryAddScoped( type );
				else
					services.TryAddScoped( attribute.ContractType, type );
			}

			foreach ( var type in GetTypesWithAttributeSorted<TransientAttribute>() )
			{
				var attribute = type.GetCustomAttribute<TransientAttribute>();

				if ( attribute.ContractType == null )
					services.TryAddTransient( type );
				else
					services.TryAddTransient( attribute.ContractType, type );
			}

			return services;
		}
	}
}