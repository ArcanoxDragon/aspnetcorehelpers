﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class ActionResultExtensions
	{
		public static JsonResult WithJson<T>( this StatusCodeResult result, T jsonData )
		{
			return new JsonResult( jsonData ) {
				ContentType = "application/json",
				StatusCode = result.StatusCode
			};
		}

		public static StatusCodeResult WithText( this StatusCodeResult result, string text )
		{
			return new StatusCodeMessageResult( result.StatusCode ) {
				Message = text
			};
		}
	}

	public class StatusCodeMessageResult : StatusCodeResult
	{
		public StatusCodeMessageResult( int statusCode ) : base( statusCode ) { }

		public string Message { get; set; }

		public override void ExecuteResult( ActionContext context )
		{
			context.HttpContext.Items[ "ErrorMessage" ] = this.Message;
			base.ExecuteResult( context );
		}

		public override Task ExecuteResultAsync( ActionContext context )
		{
			context.HttpContext.Items[ "ErrorMessage" ] = this.Message;
			return base.ExecuteResultAsync( context );
		}
	}
}