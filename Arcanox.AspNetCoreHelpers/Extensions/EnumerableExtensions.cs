﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class EnumerableExtensions
	{
		public static string Join<T>( this IEnumerable<T> source, string separator )
			=> string.Join( separator, source );

		public static IEnumerable<(int index, TSource value)> Pairs<TSource>( this IEnumerable<TSource> source )
		{
			var i = 0;

			foreach ( var value in source )
				yield return (i++, value);
		}

		public static TValue MaxOrDefault<TSource, TValue>( this ICollection<TSource> source, Func<TSource, TValue> selector, TValue defaultValue = default )
		{
			if ( !source.Any() )
				return defaultValue;

			return source.Max( selector );
		}

		public static TSource MaxOrDefault<TSource>( this ICollection<TSource> source, TSource defaultValue = default )
			=> source.MaxOrDefault( s => s, defaultValue );

		public static List<TSource> ToHierarchicalList<TSource, TKey>( this ICollection<TSource> source,
																	   Func<TSource, TKey> keyAccessor,
																	   Func<TSource, TKey> parentPropAccessor )
		{
			if ( !source.Any( s => parentPropAccessor( s ) == null ) )
				return source.ToList(); // Can't structure hierarchy due to lack of top-level parents

			var list = new List<TSource>();

			void AddWithChildren( TSource item )
			{
				list.Add( item );

				// ReSharper disable once AccessToModifiedClosure
				foreach ( var child in source.Where( s => parentPropAccessor( s ).Equals( keyAccessor( item ) ) ) )
					AddWithChildren( child );
			}

			foreach ( var item in source.Where( s => parentPropAccessor( s ) == null ) )
				AddWithChildren( item );

			if ( list.Count != source.Count )
				throw new InvalidOperationException( "Children were abandoned while structuring the hierarchy!" );

			return list;
		}

		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>( this IEnumerable<TSource> source,
																		  Func<TSource, TKey> keySelector,
																		  bool descending )
		{
			if ( descending )
				return source.OrderByDescending( keySelector );

			return source.OrderBy( keySelector );
		}

		/// <summary>
		/// Passthrough to IEnumerable.Reverse(). Workaround for List.Reverse() hiding the .Reverse() extension method.
		/// </summary>
		/// <typeparam name="TSource"></typeparam>
		/// <param name="source"></param>
		/// <returns></returns>
		public static IEnumerable<TSource> AsReverse<TSource>( this IEnumerable<TSource> source )
			=> source.Reverse();

		public static IEnumerable<TSource> NotNull<TSource>( this IEnumerable<TSource> source )
			where TSource : class
			=> source.Where( x => x != null );

		public static IEnumerable<TSource> NotNull<TSource>( this IEnumerable<TSource?> source )
			where TSource : struct
			=> source.Where( x => x != null ).Select( x => x.Value );
	}
}