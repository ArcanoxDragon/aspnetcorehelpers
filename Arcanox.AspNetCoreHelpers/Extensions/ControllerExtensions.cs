﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;

namespace Arcanox.AspNetCoreHelpers.Extensions
{
	public static class ControllerExtensions
	{
		internal static string GetActionKey( this Controller controller, [ AspMvcAction ] string action = null )
		{
			var actionDescriptor = controller.ControllerContext.ActionDescriptor;
			var controllerName   = actionDescriptor.ControllerName;
			var actionName       = action ?? actionDescriptor.ActionName;
			var actionKey        = $"{controllerName}/{actionName}";

			if ( actionDescriptor.RouteValues.TryGetValue( "area", out var area ) )
				actionKey = $"{area}/{actionKey}";

			return actionKey;
		}
	}
}