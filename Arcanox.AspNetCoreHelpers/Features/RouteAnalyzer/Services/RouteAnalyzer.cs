﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Abstract;
using Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Routing;

namespace Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Services
{
	class RouteAnalyzer : IRouteAnalyzer
	{
		private readonly IActionDescriptorCollectionProvider actionsProvider;
		private readonly IHttpContextAccessor                httpContextAccessor;

		public RouteAnalyzer( IActionDescriptorCollectionProvider actionsProvider,
							  IHttpContextAccessor httpContextAccessor )
		{
			this.actionsProvider     = actionsProvider;
			this.httpContextAccessor = httpContextAccessor;
		}

		public IEnumerable<RouteInfo> GetAllRegisteredRoutes()
		{
			var actions     = this.actionsProvider.ActionDescriptors.Items;
			var httpContext = this.httpContextAccessor.HttpContext;
			var router      = httpContext.GetRouteData().Routers.First();

			foreach ( var actionInfo in actions )
			{
				var routeInfo = new RouteInfo();

				if ( actionInfo.RouteValues.TryGetValue( "area", out var area ) )
					routeInfo.Area = area;
				if ( actionInfo.RouteValues.TryGetValue( "controller", out var controller ) )
					routeInfo.Controller = controller;
				if ( actionInfo.RouteValues.TryGetValue( "action", out var action ) )
					routeInfo.Action = action;

				var routeValues = new RouteValueDictionary( actionInfo.RouteValues );

				foreach ( var param in actionInfo.Parameters )
				{
					routeValues.TryAdd( param.Name, $"__param_{param.Name}__" );
				}

				var virtualPathContext = new VirtualPathContext( httpContext, new RouteValueDictionary(), routeValues, null );
				var virtualPath        = router.GetVirtualPath( virtualPathContext );

				if ( virtualPath?.VirtualPath != null )
				{
					var path = virtualPath.VirtualPath;

					path = Regex.Replace( path, @"(?<!=)__param_([^_]+)__", m => $"{{{m.Groups[ 1 ].Value}}}" );
					path = Regex.Replace( path, @"(?:[?&])([^?&=]+)=__param_\1__", "" );

					routeInfo.Path = path;
				}

				yield return routeInfo;
			}
		}
	}
}