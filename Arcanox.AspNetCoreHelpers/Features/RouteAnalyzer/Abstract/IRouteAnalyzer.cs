﻿using System.Collections.Generic;
using Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Models;

namespace Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Abstract
{
    public interface IRouteAnalyzer
	{
		IEnumerable<RouteInfo> GetAllRegisteredRoutes();
	}
}
