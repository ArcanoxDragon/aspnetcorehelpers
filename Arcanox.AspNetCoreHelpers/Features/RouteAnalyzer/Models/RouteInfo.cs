﻿namespace Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Models
{
	public class RouteInfo
	{
		public string Area       { get; set; }
		public string Controller { get; set; }
		public string Action     { get; set; }
		public string Path       { get; set; }
	}
}