﻿using Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Abstract;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.AspNetCoreHelpers.Features.RouteAnalyzer.Extensions
{
    public static class ServiceCollectionExtensions
    {
		public static void AddRouteAnalyzer( this IServiceCollection services )
		{
			services.AddTransient<IRouteAnalyzer, Services.RouteAnalyzer>();
		}
    }
}
