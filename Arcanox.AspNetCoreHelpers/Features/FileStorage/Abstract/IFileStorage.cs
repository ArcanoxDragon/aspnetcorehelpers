﻿using System.Threading.Tasks;

namespace Arcanox.AspNetCoreHelpers.Features.FileStorage.Abstract
{
    public interface IFileStorage
	{
		string GetFileLocation( string bucket, string filename );
		Task PutFileAsync( string bucket, string filename, byte[] data );
		Task<byte[]> GetFileAsync( string bucket, string filename );
		Task DeleteFileAsync( string bucket, string filename );
		Task<bool> FileExistsAsync( string bucket, string filename );
	}
}
