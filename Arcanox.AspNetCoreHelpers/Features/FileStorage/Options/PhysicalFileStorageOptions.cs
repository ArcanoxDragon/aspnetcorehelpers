﻿namespace Arcanox.AspNetCoreHelpers.Features.FileStorage.Options
{
    public class PhysicalFileStorageOptions
    {
        public string BasePath { get; set; }
    }
}
