﻿using System;
using Arcanox.AspNetCoreHelpers.Features.FileStorage.Abstract;
using Arcanox.AspNetCoreHelpers.Features.FileStorage.Options;
using Arcanox.AspNetCoreHelpers.Features.FileStorage.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Arcanox.AspNetCoreHelpers.Features.FileStorage.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static void AddPhysicalFileStorage( this IServiceCollection services )
		{
			services.TryAddTransient<IFileStorage, PhysicalFileStorage>();
		}

		public static void AddPhysicalFileStorage( this IServiceCollection services, Action<PhysicalFileStorageOptions> configureOptions )
		{
			services.Configure( configureOptions );
			services.TryAddTransient<IFileStorage, PhysicalFileStorage>();
		}
	}
}