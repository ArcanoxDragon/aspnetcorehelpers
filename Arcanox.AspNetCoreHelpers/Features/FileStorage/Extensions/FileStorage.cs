﻿using System.IO;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Features.FileStorage.Abstract;

namespace Arcanox.AspNetCoreHelpers.Features.FileStorage.Extensions
{
    public static class FileStorageExtensions
    {
		public static async Task<byte[]> TryGetFileAsync( this IFileStorage fileStorage, string bucket, string filename )
		{
			try
			{
				return await fileStorage.GetFileAsync( bucket, filename );
			}
			catch ( FileNotFoundException )
			{
				return null;
			}
		}
    }
}
