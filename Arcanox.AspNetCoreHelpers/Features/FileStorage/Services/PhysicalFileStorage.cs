﻿using System;
using System.IO;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Features.FileStorage.Abstract;
using Arcanox.AspNetCoreHelpers.Features.FileStorage.Options;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Options;

namespace Arcanox.AspNetCoreHelpers.Features.FileStorage.Services
{
	public class PhysicalFileStorage : IFileStorage
	{
		private readonly PhysicalFileStorageOptions options;
		private readonly IHostingEnvironment        environment;

		public PhysicalFileStorage( IOptionsSnapshot<PhysicalFileStorageOptions> options,
									IHostingEnvironment environment )
		{
			this.options     = options.Value;
			this.environment = environment;
		}

		private string GetFullPath( string bucket, string filename )
		{
			var wwwFolder = this.environment.WebRootPath;
			var basePath  = this.options.BasePath;

			if ( Path.IsPathRooted( basePath ) )
				return Path.Combine( basePath, bucket, filename );

			return Path.Combine( wwwFolder, basePath, bucket, filename );
		}

		public string GetFileLocation( string bucket, string filename ) => this.GetFullPath( bucket, filename );

		public async Task PutFileAsync( string bucket, string filename, byte[] data )
		{
			var path = this.GetFullPath( bucket, filename );
			var directory = Path.GetDirectoryName( path ) ?? throw new InvalidOperationException("Invalid image path");

			if ( !Directory.Exists( directory ) )
				Directory.CreateDirectory( directory );

			using ( var fileStream = File.Open( path, FileMode.Create, FileAccess.Write, FileShare.Read ) )
			{
				await fileStream.WriteAsync( data, 0, data.Length );
			}
		}

		public async Task<byte[]> GetFileAsync( string bucket, string filename )
		{
			var    path = this.GetFullPath( bucket, filename );
			byte[] data;

			using ( var fileStream = File.Open( path, FileMode.Open, FileAccess.Read, FileShare.Read ) )
			{
				data = new byte[ fileStream.Length ];
				await fileStream.ReadAsync( data, 0, data.Length );
			}

			return data;
		}

		public Task DeleteFileAsync( string bucket, string filename )
		{
			var path = this.GetFullPath( bucket, filename );

			if ( File.Exists( path ) )
				File.Delete( path );

			return Task.CompletedTask;
		}

		public Task<bool> FileExistsAsync( string bucket, string filename )
		{
			var path = this.GetFullPath( bucket, filename );

			return Task.FromResult( File.Exists( path ) );
		}
	}
}