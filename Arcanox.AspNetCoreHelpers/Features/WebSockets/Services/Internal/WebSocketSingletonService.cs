﻿using System.Collections.Generic;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Models;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Services.Internal
{
	class WebSocketSingletonService
	{
		public WebSocketSingletonService()
		{
			this.Connections = new List<WebSocketConnection>();
		}

		internal List<WebSocketConnection> Connections { get; }
	}
}