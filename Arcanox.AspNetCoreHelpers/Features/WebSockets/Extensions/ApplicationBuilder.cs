﻿using Arcanox.AspNetCoreHelpers.Features.WebSockets.Middleware;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Options;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Extensions
{
	public static class ApplicationBuilderExtensions
	{
		public static IApplicationBuilder UseWebSocketService( this IApplicationBuilder app )
		{
			var options = app.ApplicationServices.GetService<IOptions<WebSocketServiceOptions>>()?.Value ?? new WebSocketServiceOptions();

			app.UseWebSockets( options );
			app.UseMiddleware<WebSocketMiddleware>();

			return app;
		}
	}
}