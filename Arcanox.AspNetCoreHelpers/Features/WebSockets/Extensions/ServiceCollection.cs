﻿using System;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Abstract;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Middleware;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Options;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Services.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static void AddWebSocketService<TImpl>( this IServiceCollection services, Action<WebSocketServiceOptions> configure = null )
			where TImpl : class, IWebSocketService
		{
			services.TryAddSingleton<WebSocketSingletonService>();
			services.TryAddScoped<WebSocketMiddleware>();
			services.TryAddTransient<IWebSocketService, TImpl>();
			services.AddTransient( s => s.GetRequiredService<IWebSocketService>() as TImpl );

			if ( configure != null )
				services.Configure( configure );
		}
	}
}