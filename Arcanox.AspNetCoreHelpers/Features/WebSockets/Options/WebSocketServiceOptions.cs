﻿using Microsoft.AspNetCore.Builder;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Options
{
    public class WebSocketServiceOptions : WebSocketOptions
	{
		public string WebSocketPath { get; set; } = "/ws";
	}
}
