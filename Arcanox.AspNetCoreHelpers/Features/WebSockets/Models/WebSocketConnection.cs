﻿using System.Net;
using System.Net.WebSockets;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Models
{
	public class WebSocketConnection
	{
		public WebSocket Socket        { get; set; }
		public IPAddress RemoteAddress { get; set; }
	}
}