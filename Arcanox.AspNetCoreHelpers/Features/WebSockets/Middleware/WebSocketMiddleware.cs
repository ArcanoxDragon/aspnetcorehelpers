﻿using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Abstract;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Options;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Middleware
{
	public class WebSocketMiddleware : IMiddleware
	{
		private readonly IWebSocketService                 webSocketService;
		private readonly IOptions<WebSocketServiceOptions> options;

		public WebSocketMiddleware( IWebSocketService webSocketService, IOptions<WebSocketServiceOptions> options )
		{
			this.webSocketService = webSocketService;
			this.options          = options;
		}

		public async Task InvokeAsync( HttpContext context, RequestDelegate next )
		{
			if ( !context.WebSockets.IsWebSocketRequest )
			{
				await next( context );
				return;
			}

			if ( context.Request.Path != this.options.Value.WebSocketPath )
			{
				context.Response.StatusCode = 400;
				return;
			}

			var socket = await context.WebSockets.AcceptWebSocketAsync();

			await this.webSocketService.HandleSocketConnectedAsync( context, socket );
		}
	}
}