﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Models;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Options;
using Arcanox.AspNetCoreHelpers.Features.WebSockets.Services.Internal;
using Arcanox.AspNetCoreHelpers.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Abstract
{
	public class WebSocketEventArgs : EventArgs
	{
		[ JsonProperty( "type" ) ] public string MessageType { get; set; }

		public object Data { get; set; }
	}

	public abstract class WebSocketServiceBase : IWebSocketService
	{
		private const int DefaultBufferSize = 4 * 1024;

		private readonly WebSocketSingletonService singletonService;
		private readonly WebSocketServiceOptions   options;

		private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings {
			ContractResolver = new CamelCasePropertyNamesContractResolver()
		};

		protected WebSocketServiceBase( IServiceProvider services )
		{
			this.Services         = services;
			this.singletonService = services.GetRequiredService<WebSocketSingletonService>();
			this.options          = services.GetService<IOptions<WebSocketServiceOptions>>()?.Value ?? new WebSocketServiceOptions();

			Task.Run( this.MonitorThread ).Forget();
		}

		public event EventHandler<WebSocketEventArgs> MessageReceived;

		protected IServiceProvider          Services    { get; }
		protected List<WebSocketConnection> Connections => this.singletonService.Connections;

		public async Task HandleSocketConnectedAsync( HttpContext context, WebSocket socket )
		{
			var connection = await this.CreateConnectionAsync( context, socket );

			lock ( this.Connections )
			{
				this.Connections.Add( connection );
			}

			await this.ReceiveThread( socket );
		}

		public async Task BroadcastMessageAsync( string messageType, object data = null )
		{
			var sockets = new List<WebSocket>();

			lock ( this.Connections )
				sockets.AddRange( this.Connections.Select( c => c.Socket ) );

			foreach ( var socket in sockets )
				await this.SendToSocketAsync( socket, messageType, data );
		}

		protected virtual Task<WebSocketConnection> CreateConnectionAsync( HttpContext context, WebSocket socket )
			=> Task.FromResult( new WebSocketConnection {
				Socket        = socket,
				RemoteAddress = context.Connection.RemoteIpAddress
			} );

		protected virtual void OnSocketConnected( HttpContext context, WebSocket socket ) { }

		protected async Task SendToSocketAsync( WebSocket socket, string messageType, object data )
		{
			var json = JsonConvert.SerializeObject( new {
				Type = messageType,
				Data = data
			}, JsonSettings );
			var buffer = Encoding.UTF8.GetBytes( json );

			try
			{
				await socket.SendAsync( new ArraySegment<byte>( buffer ), WebSocketMessageType.Text, true, CancellationToken.None );
			}
			catch ( Exception ex ) when ( ex is ObjectDisposedException || ex is WebSocketException )
			{
				Console.WriteLine( "Error sending message to socket:" );
				Console.WriteLine( ex );

				lock ( this.Connections )
					this.Connections.RemoveAll( c => c.Socket == socket );
			}
		}

		private async Task ReceiveThread( WebSocket socket )
		{
			var receiving     = true;
			var buffer        = new byte[ this.options.ReceiveBufferSize ];
			var bufferSegment = new ArraySegment<byte>( buffer );

			while ( receiving )
			{
				var receiveResult = await socket.ReceiveAsync( bufferSegment, CancellationToken.None );

				if ( receiveResult.EndOfMessage )
				{
					try
					{
						var json = Encoding.UTF8.GetString( bufferSegment.Slice( 0, receiveResult.Count ).ToArray() );
						var args = JsonConvert.DeserializeObject<WebSocketEventArgs>( json, JsonSettings );

						this.MessageReceived?.Invoke( socket, args );
					}
					finally
					{
						ArrayUtilities.Fill( buffer, (byte) 0 );
						bufferSegment = new ArraySegment<byte>( buffer );
					}
				}

				if ( receiveResult.CloseStatus.HasValue )
				{
					await socket.CloseAsync( receiveResult.CloseStatus.Value, "Closed", CancellationToken.None );
					receiving = false;
				}
			}
		}

		private async Task MonitorThread()
		{
			while ( true )
			{
				var toClose = new List<WebSocket>();
				var dead    = new List<WebSocket>();

				lock ( this.Connections )
				{
					foreach ( var connection in this.Connections )
					{
						if ( connection.Socket.CloseStatus.HasValue )
							toClose.Add( connection.Socket );

						if ( connection.Socket.State != WebSocketState.Connecting &&
							 connection.Socket.State != WebSocketState.Open )
							dead.Add( connection.Socket );
					}
				}

				foreach ( var socket in toClose )
					await socket.CloseAsync( socket.CloseStatus.Value, "Socket closed", CancellationToken.None );

				foreach ( var socket in dead )
					socket.Dispose();

				lock ( this.Connections )
				{
					foreach ( var socket in toClose.Union( dead ) )
						this.Connections.RemoveAll( c => c.Socket == socket );
				}

				await Task.Delay( 5000 );
			}
		}
	}
}