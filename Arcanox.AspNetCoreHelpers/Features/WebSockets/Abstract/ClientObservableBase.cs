﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Abstract
{
	public abstract class ClientObservableBase<TClientKey, TModel> : IClientObservable<TClientKey, TModel>
	{
		private TaskCompletionSource<TModel> updateSource;

		protected ClientObservableBase( TModel initialValue = default )
		{
			this.LastCheckIns    = new Dictionary<TClientKey, DateTime>();
			this.LastUpdatedTime = DateTime.Now;
			this.CurrentValue    = initialValue;

			this.updateSource = new TaskCompletionSource<TModel>();
		}

		protected IDictionary<TClientKey, DateTime> LastCheckIns { get; }

		public DateTime LastUpdatedTime { get; protected set; }
		public TModel   CurrentValue    { get; protected set; }

		public async Task<TModel> WaitForChangeAsync( TClientKey clientKey )
		{
			var checkedInBefore = this.LastCheckIns.TryGetValue( clientKey, out var lastCheckIn );

			this.LastCheckIns[ clientKey ] = DateTime.Now;

			if ( !checkedInBefore ||
				 lastCheckIn < this.LastUpdatedTime )
				// If they've never checked in before, or the
				// value has changed since last check-in, give
				// it immediately
				return this.CurrentValue;

			// Otherwise, the value is fresh and we should wait for it to update again
			return await this.updateSource.Task;
		}

		public void NotifyChange( TModel newModel )
		{
			this.CurrentValue    = newModel;
			this.LastUpdatedTime = DateTime.Now;

			// Notify waiting clients of the change
			this.updateSource.SetResult( newModel );
			this.updateSource = new TaskCompletionSource<TModel>();
		}
	}
}