﻿using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Abstract
{
	public interface IWebSocketService
	{
		Task HandleSocketConnectedAsync( HttpContext context, WebSocket socket );
	}
}