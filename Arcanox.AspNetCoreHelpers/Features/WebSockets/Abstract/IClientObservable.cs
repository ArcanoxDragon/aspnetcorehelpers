﻿using System.Threading.Tasks;

namespace Arcanox.AspNetCoreHelpers.Features.WebSockets.Abstract
{
	public interface IClientObservable<in TClientKey, TModel>
	{
		Task<TModel> WaitForChangeAsync( TClientKey clientKey );
		void NotifyChange( TModel newModel );
	}
}