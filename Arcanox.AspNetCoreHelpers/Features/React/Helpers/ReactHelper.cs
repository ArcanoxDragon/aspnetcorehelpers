﻿using System;
using Arcanox.AspNetCoreHelpers.Attributes;
using Arcanox.AspNetCoreHelpers.Features.React.Options;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Arcanox.AspNetCoreHelpers.Features.React.Helpers
{
	[ Transient ]
	public class ReactHelper : IViewContextAware
	{
		private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings {
			ContractResolver      = new CamelCasePropertyNamesContractResolver(),
			ReferenceLoopHandling = ReferenceLoopHandling.Ignore
		};

		private static readonly ReactOptions DefaultOptions = new ReactOptions();

		private readonly IHtmlHelper  html;
		private readonly ReactOptions options;

		public ReactHelper( IHtmlHelper html, IServiceProvider services )
		{
			this.html    = html;
			this.options = services.GetService<IOptions<ReactOptions>>()?.Value ?? DefaultOptions;
		}

		public IHtmlContent App( string appName, object props = null )
		{
			var httpContext     = this.html.ViewContext.HttpContext;
			var contentBuilder  = (IHtmlContentBuilder) new HtmlContentBuilder();
			var reactTagBuilder = new TagBuilder( "div" );

			reactTagBuilder.Attributes.Add( "data-app-name", appName );

			if ( props != null )
			{
				var propsJson = JsonConvert.SerializeObject( props, JsonSettings );

				reactTagBuilder.Attributes.Add( "data-props", propsJson );
			}

			if ( httpContext.Items[ "_ReactEmittedLibs" ] as bool? != true )
			{
				httpContext.Items[ "_ReactEmittedLibs" ] = true;

				var scriptTagBuilder = new TagBuilder( "script" );

				scriptTagBuilder.Attributes.Add( "src",   this.options.ReactEntryPointFile );
				scriptTagBuilder.Attributes.Add( "async", "async" );
				scriptTagBuilder.Attributes.Add( "defer", "defer" );

				contentBuilder = contentBuilder.AppendHtml( scriptTagBuilder );
			}

			return contentBuilder.AppendHtml( reactTagBuilder );
		}

		void IViewContextAware.Contextualize( ViewContext viewContext )
		{
			( (IViewContextAware) this.html ).Contextualize( viewContext );
		}
	}
}