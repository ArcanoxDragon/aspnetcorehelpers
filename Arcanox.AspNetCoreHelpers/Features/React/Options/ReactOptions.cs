﻿namespace Arcanox.AspNetCoreHelpers.Features.React.Options
{
	public class ReactOptions
	{
		public string ReactEntryPointFile { get; set; } = "/js/entry/react-mount.js";
	}
}