﻿using System;
using System.IO;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Attributes;
using Arcanox.AspNetCoreHelpers.Features.ViewRendering.Abstract;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;

namespace Arcanox.AspNetCoreHelpers.Features.ViewRendering.Services
{
	[ Scoped( typeof( IViewRenderService ) ) ]
	public class ViewRenderService : IViewRenderService
	{
		private readonly IRazorViewEngine  razorViewEngine;
		private readonly ITempDataProvider tempDataProvider;
		private readonly IServiceProvider  serviceProvider;

		public ViewRenderService( IRazorViewEngine razorViewEngine,
								  ITempDataProvider tempDataProvider,
								  IServiceProvider serviceProvider )
		{
			this.razorViewEngine  = razorViewEngine;
			this.tempDataProvider = tempDataProvider;
			this.serviceProvider  = serviceProvider;
		}

		public async Task<string> RenderViewAsync( [ AspMvcView ] string viewName, object model, HttpContext renderHttpContext = null )
		{
			var httpContext   = renderHttpContext ?? new DefaultHttpContext { RequestServices = this.serviceProvider };
			var routeData     = httpContext.GetRouteData() ?? new RouteData();
			var actionContext = new ActionContext( httpContext, routeData, new ActionDescriptor() );

			using ( var sw = new StringWriter() )
			{
				var viewResult = this.razorViewEngine.GetView( null, viewName, true );

				if ( !viewResult.Success )
					viewResult = this.razorViewEngine.FindView( actionContext, viewName, true );

				if ( !viewResult.Success || viewResult.View == null )
					throw new FileNotFoundException( $"{viewName} does not match any available view" );

				var metadataProvider  = new EmptyModelMetadataProvider();
				var modelState        = new ModelStateDictionary();
				var tempData          = new TempDataDictionary( actionContext.HttpContext, this.tempDataProvider );
				var htmlHelperOptions = new HtmlHelperOptions();
				var viewDictionary = new ViewDataDictionary( metadataProvider, modelState ) {
					Model = model
				};
				var viewContext = new ViewContext(
					actionContext,
					viewResult.View,
					viewDictionary,
					tempData,
					sw,
					htmlHelperOptions
				);

				await viewResult.View.RenderAsync( viewContext );

				return sw.ToString();
			}
		}
	}
}