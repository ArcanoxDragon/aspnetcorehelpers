﻿using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;

namespace Arcanox.AspNetCoreHelpers.Features.ViewRendering.Abstract
{
	public interface IViewRenderService
	{
		Task<string> RenderViewAsync( [AspMvcView] string viewName, object model, HttpContext renderHttpContext = null );
	}
}