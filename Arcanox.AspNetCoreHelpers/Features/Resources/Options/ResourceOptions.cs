﻿namespace Arcanox.AspNetCoreHelpers.Features.Resources.Options
{
	public class ResourceOptions
	{
		public string PageStyleBaseUrl { get; set; } = "/css";
		public string LibStyleBaseUrl  { get; set; } = "/lib";
		public string PageScriptBaseUrl { get; set; } = "/js";
		public string LibScriptBaseUrl  { get; set; } = "/lib";
	}
}