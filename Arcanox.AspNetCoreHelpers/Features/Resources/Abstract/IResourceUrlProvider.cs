﻿namespace Arcanox.AspNetCoreHelpers.Features.Resources.Abstract
{
    public interface IResourceUrlProvider
    {
		string VersionedFile( string fileUrl );
		string ScriptUrl( ResourceType type, string scriptName, bool versioned );
		string StyleUrl( ResourceType type, string styleName, bool versioned );
	}
}
