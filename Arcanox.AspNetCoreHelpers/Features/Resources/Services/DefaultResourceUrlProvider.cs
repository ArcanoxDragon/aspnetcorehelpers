﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using Arcanox.AspNetCoreHelpers.Attributes;
using Arcanox.AspNetCoreHelpers.Features.Resources.Abstract;
using Arcanox.AspNetCoreHelpers.Features.Resources.Options;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Arcanox.AspNetCoreHelpers.Features.Resources.Services
{
	[ Transient( typeof( IResourceUrlProvider ), Priority = int.MinValue ) ]
	public class DefaultResourceUrlProvider : IResourceUrlProvider
	{
		private static readonly ResourceOptions DefaultOptions = new ResourceOptions();

		public DefaultResourceUrlProvider(
			IServiceProvider services,
			IHostingEnvironment hostingEnvironment
		)
		{
			var configuredOptions = services.GetService<IOptions<ResourceOptions>>();

			this.Options            = configuredOptions?.Value ?? DefaultOptions;
			this.HostingEnvironment = hostingEnvironment;
		}

		protected ResourceOptions     Options            { get; }
		protected IHostingEnvironment HostingEnvironment { get; }

		public string VersionedFile( string fileUrl )
		{
			var fixedPath = Regex.Match( fileUrl, @"/?(.*)" ).Groups[ 1 ].Value;
			var fullPath  = Path.Combine( this.HostingEnvironment.WebRootPath, fixedPath );

			if ( !File.Exists( fullPath ) )
				return fileUrl;

			var modificationTime = File.GetLastWriteTime( fullPath );

			return $"{fileUrl}?_={modificationTime.Ticks}";
		}

		public string ScriptUrl( ResourceType type, string scriptName, bool versioned )
		{
			string url;

			switch ( type )
			{
				case ResourceType.Lib:
					url = $"{this.Options.LibScriptBaseUrl}/{scriptName}.js";
					break;
				case ResourceType.Page:
					url = $"{this.Options.PageScriptBaseUrl}/{scriptName}.js";
					break;
				default: throw new ApplicationException();
			}

			if ( versioned )
				url = this.VersionedFile( url );

			return url;
		}

		public string StyleUrl( ResourceType type, string styleName, bool versioned )
		{
			string url;

			switch ( type )
			{
				case ResourceType.Lib:
					url = $"{this.Options.LibStyleBaseUrl}/{styleName}.css";
					break;
				case ResourceType.Page:
					url = $"{this.Options.PageStyleBaseUrl}/{styleName}.css";
					break;
				default: throw new ApplicationException();
			}

			if ( versioned )
				url = this.VersionedFile( url );

			return url;
		}
	}
}