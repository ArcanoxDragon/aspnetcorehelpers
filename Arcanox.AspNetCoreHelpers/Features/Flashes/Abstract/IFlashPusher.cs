﻿namespace Arcanox.AspNetCoreHelpers.Features.Flashes.Abstract
{
	public interface IFlashPusher
	{
		void Success( string message, bool html = false, object data = null );
		void Info( string message, bool html = false, object data = null );
		void Error( string message, bool html = false, object data = null );
		void Message( string type, string message, bool html = false, object data = null );
	}
}