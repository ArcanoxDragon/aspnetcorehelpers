﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Arcanox.AspNetCoreHelpers.Features.Flashes.Models
{
	public class FlashCollection
	{
		public FlashCollection()
		{
			this.Flashes = new Dictionary<string, IList<FlashMessage>>( StringComparer.OrdinalIgnoreCase );
		}

		public Dictionary<string, IList<FlashMessage>> Flashes { get; set; }

		public void PushMessage( FlashMessage message )
		{
			lock ( this.Flashes )
			{
				if ( !this.Flashes.TryGetValue( message.Type, out IList<FlashMessage> list ) )
					this.Flashes.Add( message.Type, list = new List<FlashMessage>() );

				list.Add( message );
			}
		}

		public IEnumerable<FlashMessage> PopMessages( string type )
		{
			lock ( this.Flashes )
			{
				if ( this.Flashes.TryGetValue( type, out IList<FlashMessage> list ) )
				{
					this.Flashes[ type ] = new List<FlashMessage>();
					return list;
				}
			}

			return Enumerable.Empty<FlashMessage>();
		}

		public IReadOnlyDictionary<string, IEnumerable<FlashMessage>> PopMessages()
		{
			var returnDictionary = new Dictionary<string, IEnumerable<FlashMessage>>();

			lock ( this.Flashes )
				foreach ( var type in this.Flashes.Keys )
					returnDictionary[ type ] = this.Flashes[ type ].ToList();

			lock ( this.Flashes )
				this.Flashes = new Dictionary<string, IList<FlashMessage>>();

			return returnDictionary;
		}
	}
}