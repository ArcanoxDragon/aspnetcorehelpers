﻿namespace Arcanox.AspNetCoreHelpers.Features.Flashes.Models
{
	public class FlashMessage
	{
		public string Type    { get; set; }
		public string Message { get; set; }
		public bool   IsHtml  { get; set; }
		public object Data    { get; set; }
	}
}