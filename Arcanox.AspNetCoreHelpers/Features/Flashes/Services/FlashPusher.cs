﻿using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.Flashes.Abstract;
using Arcanox.AspNetCoreHelpers.Features.Flashes.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.AspNetCoreHelpers.Features.Flashes.Services
{
	class FlashPusher : IFlashPusher
	{
		private readonly IHttpContextAccessor httpContextAccessor;

		public FlashPusher( IHttpContextAccessor httpContextAccessor )
		{
			this.httpContextAccessor = httpContextAccessor;
		}

		public void Success( string message, bool html = false, object data = null ) => this.Message( "success", message, html, data );
		public void Info( string message, bool html = false, object data = null ) => this.Message( "info",       message, html, data );
		public void Error( string message, bool html = false, object data = null ) => this.Message( "error",     message, html, data );

		public void Message( string type, string message, bool html = false, object data = null )
		{
			var httpContext     = this.httpContextAccessor.HttpContext;
			var tempDataFactory = httpContext.RequestServices.GetService<ITempDataDictionaryFactory>();
			var tempData        = tempDataFactory.GetTempData( httpContext );
			var flashCollection = tempData.Get<FlashCollection>( "Flashes" ) ?? new FlashCollection();

			flashCollection.PushMessage( new FlashMessage {
				Type    = type,
				Message = message,
				IsHtml  = html,
				Data    = data,
			} );

			tempData.Put( "Flashes", flashCollection );
		}
	}
}