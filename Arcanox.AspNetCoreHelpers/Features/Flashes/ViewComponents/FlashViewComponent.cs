﻿#pragma warning disable 1998

using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.Flashes.Models;
using Microsoft.AspNetCore.Mvc;

namespace Arcanox.AspNetCoreHelpers.Features.Flashes.ViewComponents
{
	[ ViewComponent( Name = "Flashes" ) ]
	[ SuppressMessage( "ReSharper", "ArrangeThisQualifier" ) ]
	public class FlashViewComponent : ViewComponent
	{
		public async Task<IViewComponentResult> InvokeAsync()
		{
			var flashCollection = this.TempData.Get<FlashCollection>( "Flashes" ) ?? new FlashCollection();
			var flashes         = flashCollection.PopMessages();

			return this.View( flashes );
		}
	}
}