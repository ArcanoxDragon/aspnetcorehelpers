﻿using Arcanox.AspNetCoreHelpers.Features.Flashes.Abstract;
using Arcanox.AspNetCoreHelpers.Features.Flashes.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.AspNetCoreHelpers.Features.Flashes.Extensions
{
	public static class ServiceCollectionExtensions
	{
		public static void AddFlashes( this IServiceCollection services )
		{
			services.AddTransient<IFlashPusher, FlashPusher>();
		}
	}
}