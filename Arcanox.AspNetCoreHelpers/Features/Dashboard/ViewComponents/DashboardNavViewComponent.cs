﻿using System.Diagnostics.CodeAnalysis;
using Arcanox.AspNetCoreHelpers.Features.Dashboard.Abstract;
using Arcanox.AspNetCoreHelpers.Features.Dashboard.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace Arcanox.AspNetCoreHelpers.Features.Dashboard.ViewComponents
{
	[ SuppressMessage( "ReSharper", "ArrangeThisQualifier" ) ]
	public class DashboardNavViewComponent : ViewComponent
	{
		public IViewComponentResult Invoke( DashboardPageProvider pageProvider )
			=> View( new DashboardViewModel {
				Pages = pageProvider.GetPages()
			} );
	}
}