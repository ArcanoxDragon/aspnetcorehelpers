﻿using System;
using System.Collections.Generic;
using Arcanox.AspNetCoreHelpers.Features.Dashboard.Models;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.AspNetCoreHelpers.Features.Dashboard.Abstract
{
	public abstract class DashboardPageProvider : IViewContextAware
	{
		protected ViewContext ViewContext { get; private set; }

		public abstract IEnumerable<DashboardPage> GetPages();

		public void Contextualize( ViewContext viewContext )
		{
			this.ViewContext = viewContext;
		}

		protected DashboardPage Page( string name, [ AspMvcAction ] string action, [ AspMvcController ] string controller, [ AspMvcArea ] string area = null )
		{
			if ( this.ViewContext == null )
				throw new InvalidOperationException( $"{nameof(DashboardPageProvider)} has not yet been contextualized" );

			var urlHelperFactory = this.ViewContext.HttpContext.RequestServices.GetRequiredService<IUrlHelperFactory>();
			var urlHelper        = urlHelperFactory.GetUrlHelper( this.ViewContext );
			var pageUrl          = urlHelper.Action( action, controller, new { area } );

			return new DashboardPage {
				Name = name,
				Url  = pageUrl
			};
		}
	}
}