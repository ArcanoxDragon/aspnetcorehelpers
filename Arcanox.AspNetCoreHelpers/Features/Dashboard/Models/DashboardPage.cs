﻿namespace Arcanox.AspNetCoreHelpers.Features.Dashboard.Models
{
	public class DashboardPage
	{
		public string IconName    { get; set; }
		public string Name        { get; set; }
		public string Description { get; set; }
		public string Url         { get; set; }

		public DashboardPage WithDescription( string description )
		{
			this.Description = description;
			return this;
		}

		public DashboardPage WithIcon( string icon )
		{
			this.IconName = icon;
			return this;
		}
	}
}