﻿using System.Collections.Generic;

namespace Arcanox.AspNetCoreHelpers.Features.Dashboard.Models.ViewModels
{
    public class DashboardViewModel
    {
        public IEnumerable<DashboardPage> Pages { get; set; }
    }
}
