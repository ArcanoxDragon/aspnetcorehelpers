﻿using System.Linq;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.Table.Fluent;
using Arcanox.AspNetCoreHelpers.Features.Table.Models;
using Arcanox.AspNetCoreHelpers.Features.Table.Models.ViewModels;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;

namespace Arcanox.AspNetCoreHelpers.Features.Table.Extensions
{
	public static class TableControllerExtensions
	{
		internal static string GetTableRestoreKey( this Controller controller, [ AspMvcAction ] string action = null )
			=> $"table:{controller.GetActionKey( action )}:restore";

		public static TController RestoreNextTable<TController>( this TController controller, [ AspMvcAction ] string action = null )
			where TController : Controller
		{
			controller.TempData.Put( controller.GetTableRestoreKey( action ), (object) true );

			return controller;
		}

		public static IActionResult RestoreTableAndRedirect<TController>( this TController controller, [ AspMvcAction ] string action = null )
			where TController : Controller
			=> controller.RestoreNextTable( action ).RedirectToAction( action );

		[ AspMvcView ]
		public static TableViewBuilder<IActionResult, TModel, TViewModel> TableView<TModel, TViewModel>( this Controller controller, IQueryable<TModel> collection, TViewModel model, [ AspMvcView ] string viewName = null )
			where TViewModel : TableViewModel<TModel>
			where TModel : class
		{
			var tableViewProvider = new ControllerTableViewProvider( controller );
			var tableViewBuilder  = new TableViewBuilder<IActionResult, TModel, TViewModel>( tableViewProvider, viewName, collection ).WithViewModel( model );

			return tableViewBuilder;
		}

		[ AspMvcView ]
		public static TableViewBuilder<IActionResult, TModel, TableViewModel<TModel>> TableView<TModel>( this Controller controller, IQueryable<TModel> collection, [ AspMvcView ] string viewName = null )
			where TModel : class
		{
			return controller.TableView<TModel, TableViewModel<TModel>>( collection, null, viewName );
		}

		[ AspMvcView ]
		public static TableViewBuilder<IViewComponentResult, TModel, TViewModel> TableView<TModel, TViewModel>( this ViewComponent component, IQueryable<TModel> collection, TViewModel model, [ AspMvcView ] string viewName = null )
			where TViewModel : TableViewModel<TModel>
			where TModel : class
		{
			var tableViewProvider = new ViewComponentTableViewProvider( component );
			var tableViewBuilder  = new TableViewBuilder<IViewComponentResult, TModel, TViewModel>( tableViewProvider, viewName, collection ).WithViewModel( model );

			return tableViewBuilder;
		}

		[ AspMvcView ]
		public static TableViewBuilder<IViewComponentResult, TModel, TableViewModel<TModel>> TableView<TModel>( this ViewComponent component, IQueryable<TModel> collection, [ AspMvcView ] string viewName = null )
			where TModel : class
		{
			return component.TableView<TModel, TableViewModel<TModel>>( collection, null, viewName );
		}
	}
}