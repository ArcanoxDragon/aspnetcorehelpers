﻿using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Features.Table.Models;
using Arcanox.AspNetCoreHelpers.Features.Table.Models.ViewModels;

namespace Arcanox.AspNetCoreHelpers.Features.Table.Extensions
{
	public static class TableViewModelExtensions
	{
		public static PageableDataSet<TModel> ToDataSet<TModel>( this TableViewModel<TModel> viewModel )
			=> new PageableDataSet<TModel>( viewModel.Collection, viewModel.PerPage );

		public static IPageableViewQueryable<TModel> GetView<TModel>( this TableViewModel<TModel> viewModel )
			=> viewModel.ToDataSet().GetView( viewModel.CurPage );

		public static Task<IPageableViewQueryable<TModel>> GetViewAsync<TModel>( this TableViewModel<TModel> viewModel )
			=> viewModel.ToDataSet().GetViewAsync( viewModel.CurPage );
	}
}