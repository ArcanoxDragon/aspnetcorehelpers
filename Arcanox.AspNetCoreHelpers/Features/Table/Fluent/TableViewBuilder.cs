﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.Table.Models;
using Arcanox.AspNetCoreHelpers.Features.Table.Models.ViewModels;
using Arcanox.AspNetCoreHelpers.Utilities;
using Arcanox.AspNetCoreHelpers.Utilities.Linq;
using ExpressionHelper = Arcanox.AspNetCoreHelpers.Utilities.Linq.ExpressionHelper;

namespace Arcanox.AspNetCoreHelpers.Features.Table.Fluent
{
	public class TableViewBuilder<TViewResult, TModel, TViewModel>
	{
		public TableViewBuilder( ITableViewProvider<TViewResult> tableViewProvider, string viewName, IQueryable<TModel> collection )
		{
			this.TableViewProvider = tableViewProvider;
			this.ViewName          = viewName;
			this.Collection        = collection;
		}

		public ITableViewProvider<TViewResult> TableViewProvider { get; }
		public string                          ViewName          { get; }
		public IQueryable<TModel>              Collection        { get; }

		public TViewModel                  ViewModel             { get; private set; }
		public TableSortDictionary<TModel> SortDictionary        { get; private set; }
		public string                      DefaultSort           { get; private set; }
		public bool                        DefaultSortDescending { get; private set; }
		public ISearchProvider<TModel>     CustomSearchProvider  { get; private set; }

		public Expression<Func<TViewModel, TableViewModel<TModel>>> TableModelExpression { get; private set; }

		public TViewResult Build()
		{
			var collection = this.Collection;

			if ( collection == null )
				return this.GetViewResult();

			var defaultTableState = new TableState {
				SortBy   = this.DefaultSort,
				SortDesc = this.DefaultSortDescending,
				Page     = 1,
				Search   = string.Empty
			};

			if ( this.TableViewProvider.TryRestoreTable( defaultTableState, out var restoreResult ) )
				return restoreResult;

			var query = this.TableViewProvider.Request.Query;

			var sortBy = query[ "sortBy" ].ToString();
			var search = query[ "search" ];

			if ( sortBy.IsNullOrWhiteSpace() )
				sortBy = this.DefaultSort;

			if ( !bool.TryParse( query[ "sortDesc" ], out var sortDesc ) )
				sortDesc = this.DefaultSortDescending;

			if ( !int.TryParse( query[ "page" ], out var page ) )
				page = 1;

			if ( !sortBy.IsNullOrEmpty() )
			{
				if ( this.SortDictionary != null && this.SortDictionary.TryGetValue( sortBy, out var sortExpression ) )
				{
					collection = collection.OrderBy( sortExpression, sortDesc );
				}
				else
				{
					try
					{
						var dynamicSortExpression = ExpressionHelper.GetExpression<TModel>( sortBy );
						
						collection = collection.OrderByExpression( dynamicSortExpression, sortDesc );
					}
					catch ( InvalidOperationException )
					{
						// ignored
					}
				}
			}

			if ( !search.IsNullOrEmpty() )
			{
				var searchProvider = this.CustomSearchProvider ?? this.FindSearchProvider();

				collection = searchProvider.Search( collection, search );
			}

			this.TableViewProvider.ViewData.Put( "SortBy",         sortBy );
			this.TableViewProvider.ViewData.Put( "SortDescending", sortDesc );

			var tableModel = this.GetOrCreateTableViewModel();

			tableModel.Collection     = collection;
			tableModel.SortMember     = sortBy;
			tableModel.SortDescending = sortDesc;
			tableModel.CurPage        = page;
			tableModel.Search         = search;

			var tableState = new TableState {
				SortBy   = sortBy,
				SortDesc = sortDesc,
				Page     = page,
				Search   = search
			};

			this.TableViewProvider.TryPersistTable( tableState );

			return this.GetViewResult();
		}

		#region Fluent API

		public TableViewBuilder<TViewResult, TModel, TNewViewModel> WithViewModel<TNewViewModel>(
			TNewViewModel newViewModel,
			Expression<Func<TNewViewModel, TableViewModel<TModel>>> tableModelExpression = null )
		{
			return new TableViewBuilder<TViewResult, TModel, TNewViewModel>(
				this.TableViewProvider,
				this.ViewName,
				this.Collection ) {
				ViewModel             = newViewModel,
				SortDictionary        = this.SortDictionary,
				DefaultSort           = this.DefaultSort,
				DefaultSortDescending = this.DefaultSortDescending,
				CustomSearchProvider  = this.CustomSearchProvider,
				TableModelExpression  = tableModelExpression
			};
		}

		public TableViewBuilder<TViewResult, TModel, TViewModel> WithSortDictionary( TableSortDictionary<TModel> sortDictionary )
		{
			this.SortDictionary = sortDictionary;

			return this;
		}

		public TableViewBuilder<TViewResult, TModel, TViewModel> WithDefaultSort( string defaultSort )
		{
			this.DefaultSort = defaultSort;

			return this;
		}

		public TableViewBuilder<TViewResult, TModel, TViewModel> WithDefaultSortDirection( bool defaultSortDescending )
		{
			this.DefaultSortDescending = defaultSortDescending;

			return this;
		}

		public TableViewBuilder<TViewResult, TModel, TViewModel> WithSearchProvider( ISearchProvider<TModel> searchProvider )
		{
			this.CustomSearchProvider = searchProvider;

			return this;
		}

		public TableViewBuilder<TViewResult, TModel, TViewModel> WithTableModelExpression( Expression<Func<TViewModel, TableViewModel<TModel>>> tableModelExpression )
		{
			this.TableModelExpression = tableModelExpression;

			return this;
		}

		#endregion

		private TViewResult GetViewResult()
		{
			if ( this.ViewName.IsNullOrEmpty() )
				return this.TableViewProvider.View( this.ViewModel );

			return this.TableViewProvider.View( this.ViewName, this.ViewModel );
		}

		private ISearchProvider<TModel> FindSearchProvider()
		{
			var modelType   = typeof( TModel );
			var genericType = typeof( ISearchProvider<> ).MakeGenericType( modelType );
			var types = ( from assembly in AppDomain.CurrentDomain.GetAssemblies()
						  from type in assembly.DefinedTypes
						  where type.GetCustomAttributes<SearchProviderAttribute>().Any()
						  where genericType.IsAssignableFrom( type )
						  select type ).ToList();

			if ( !types.Any() )
				throw new InvalidOperationException( $"Cannot perform search on IQueryable<{modelType.Name}> because " +
													 $"a type implementing ISearchProvider<{modelType.Name}> could not be found" );

			if ( types.Count > 1 )
				throw new InvalidOperationException( $"Cannot perform search on IQueryable<{modelType.Name}> because " +
													 $"more than one type implementing ISearchProvider<{modelType.Name}> was found. " +
													 $"Specify the implementation of ISearchProvider<{modelType.Name}> explicitly." );

			return Activator.CreateInstance( types.Single() ) as ISearchProvider<TModel>;
		}

		private TableViewModel<TModel> GetOrCreateTableViewModel()
		{
			TableViewModel<TModel> tableModel;

			if ( this.TableModelExpression != null )
			{
				if ( this.ViewModel == null )
					throw new InvalidOperationException( $"{nameof(this.ViewModel)} cannot be null when a custom {nameof(this.TableModelExpression)} is provided" );

				var tableModelGetter = this.TableModelExpression.Compile();

				tableModel = tableModelGetter( this.ViewModel );

				if ( tableModel == null )
				{
					if ( this.TableModelExpression.Body is MemberExpression memberExpression )
					{
						var tableModelProperty = memberExpression.Member as PropertyInfo;

						if ( tableModelProperty?.CanWrite != true || !ActivatorHelper.TryCreateInstance( out tableModel ) )
							throw new InvalidOperationException( "ViewModel's TableViewModel was null and could not be initialized. Action cannot continue." );

						tableModelProperty.SetValue( this.ViewModel, tableModel );
					}
					else
					{
						throw new InvalidOperationException( "ViewModel's TableViewModel was null and could not be initialized. Action cannot continue." );
					}
				}
			}
			else if ( this.ViewModel is TableViewModel<TModel> viewModelAsTableModel )
			{
				tableModel = viewModelAsTableModel;
			}
			else if ( object.Equals( this.ViewModel, default( TViewModel ) ) && typeof( TableViewModel<TModel> ).IsAssignableFrom( typeof( TViewModel ) ) )
			{
				this.ViewModel = Activator.CreateInstance<TViewModel>();
				tableModel     = this.ViewModel as TableViewModel<TModel>;
			}
			else
			{
				throw new InvalidOperationException( "ViewModel is not a TableViewModel and a custom TableModelExpression was not provided. Action cannot continue." );
			}

			return tableModel;
		}
	}
}