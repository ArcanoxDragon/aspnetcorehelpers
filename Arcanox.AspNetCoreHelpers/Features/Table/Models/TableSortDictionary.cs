﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Arcanox.AspNetCoreHelpers.Features.Table.Models
{
	public class TableSortDictionary<TModel> : Dictionary<string, Expression<Func<TModel, object>>> { }
}