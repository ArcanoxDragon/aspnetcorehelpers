﻿using System.Linq;

namespace Arcanox.AspNetCoreHelpers.Features.Table.Models.ViewModels
{
	public class TableViewModel<T> : PageableViewModel
	{
		public string SortMember { get; set; }
		public bool SortDescending { get; set; }
		public string Search { get; set; }
		public IQueryable<T> Collection { get; set; }
	}
}