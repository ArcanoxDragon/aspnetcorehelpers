﻿namespace Arcanox.AspNetCoreHelpers.Features.Table.Models.ViewModels
{
	public class PaginatorViewModel
	{
		public string[] ExcludeQueryParams { get; set; } = new string[ 0 ];
		public int      CurrentPage        { get; set; }
		public int      TotalPages         { get; set; }
	}
}