﻿namespace Arcanox.AspNetCoreHelpers.Features.Table.Models.ViewModels
{
    public class PageableViewModel
	{
		public int CurPage { get; set; } = 1;
		public int PerPage { get; set; } = 10;
	}
}
