﻿using System.Linq;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.Table.Extensions;
using Arcanox.AspNetCoreHelpers.Utilities.Json;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Routing;
using Newtonsoft.Json;

namespace Arcanox.AspNetCoreHelpers.Features.Table.Models
{
	public interface ITableViewProvider<TResult>
	{
		HttpRequest        Request  { get; }
		ViewDataDictionary ViewData { get; }

		TResult View( object viewModel = null );
		TResult View( [ AspMvcView ] string viewName, object viewModel = null );

		bool TryRestoreTable( TableState defaultTableState, out TResult restoreResult );
		void TryPersistTable( TableState tableState );
	}

	public class ControllerTableViewProvider : ITableViewProvider<IActionResult>
	{
		private readonly Controller controller;

		public ControllerTableViewProvider( Controller controller )
		{
			this.controller = controller;
		}

		public HttpRequest        Request  => this.controller.Request;
		public ViewDataDictionary ViewData => this.controller.ViewData;

		public IActionResult View( object viewModel = null )
			=> this.controller.View( viewModel );

		public IActionResult View( [ AspMvcView ] string viewName, object viewModel = null )
			=> this.controller.View( viewName, viewModel );

		public bool TryRestoreTable( TableState defaultTableState, out IActionResult restoreResult )
		{
			var actionKey = this.controller.GetActionKey();
			var session   = this.controller.HttpContext.Session;
			var query     = this.controller.Request.Query;

			if ( this.controller.TempData.TryGetValue( this.controller.GetTableRestoreKey(), out var oRestoreTable ) && oRestoreTable is "true" )
			{
				// Restore route values from session if possible

				bool TryRestore( out object routeValues )
				{
					var tableStateJson = session?.GetString( $"table:{actionKey}" );

					routeValues = null;

					if ( tableStateJson == null )
						return false;

					if ( !JsonUtilities.TryDeserializeObject<TableState>( tableStateJson, out var tableState ) )
						return false;

					var  sortBy = (string) query[ "sortBy" ] ?? tableState.SortBy ?? defaultTableState.SortBy;
					var  search = (string) query[ "search" ] ?? tableState.Search;
					bool sortDesc;
					int  page;

					if ( tableState.SortDesc != null )
						sortDesc = (bool) tableState.SortDesc;
					else if ( !bool.TryParse( query[ "sortDesc" ], out sortDesc ) )
						sortDesc = (bool) defaultTableState.SortDesc;

					if ( tableState.Page != null )
						page = (int) tableState.Page;
					else if ( !int.TryParse( query[ "page" ], out page ) )
						page = 1;

					routeValues = new { sortBy, search, sortDesc, page };
					return true;
				}

				if ( TryRestore( out var restoreRouteValues ) )
				{
					var actionDescriptor = this.controller.ControllerContext.ActionDescriptor;
					var oldRouteValues   = actionDescriptor.RouteValues.ToDictionary( pair => pair.Key, pair => (object) pair.Value );
					var newRouteValues   = new RouteValueDictionary( restoreRouteValues );

					this.controller.TempData.Remove( this.controller.GetTableRestoreKey() );

					restoreResult = this.controller.RedirectToAction( actionDescriptor.ActionName, actionDescriptor.ControllerName, oldRouteValues.MergeWith( newRouteValues ).ToObject() );
					return true;
				}
			}

			restoreResult = default;
			return false;
		}

		public void TryPersistTable( TableState tableState )
		{
			if ( this.controller.HttpContext.Session != null )
			{
				// Persist table query state for restore functionality
				var actionKey = this.controller.GetActionKey();
				var session   = this.controller.HttpContext.Session;

				session.SetString( $"table:{actionKey}", JsonConvert.SerializeObject( tableState ) );
			}
		}
	}

	public class ViewComponentTableViewProvider : ITableViewProvider<IViewComponentResult>
	{
		private readonly ViewComponent viewComponent;

		public ViewComponentTableViewProvider( ViewComponent viewComponent )
		{
			this.viewComponent = viewComponent;
		}

		public HttpRequest        Request  => this.viewComponent.Request;
		public ViewDataDictionary ViewData => this.viewComponent.ViewData;

		public IViewComponentResult View( object viewModel = null )
			// ReSharper disable once Mvc.ViewComponentViewNotResolved
			=> this.viewComponent.View( viewModel );

		public IViewComponentResult View( [ AspMvcView ] string viewName, object viewModel = null )
			=> this.viewComponent.View( viewName, viewModel );

		public bool TryRestoreTable( TableState defaultTableState, out IViewComponentResult restoreResult )
		{
			restoreResult = default;
			return false;
		}

		public void TryPersistTable( TableState tableState ) { }
	}
}