﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Arcanox.AspNetCoreHelpers.Features.Table.Models
{
	public class PageableDataSet<T>
	{
		public PageableDataSet( IQueryable<T> collection, int perPage )
		{
			this.Source  = collection;
			this.PerPage = perPage;
		}

		public IQueryable<T> Source  { get; }
		public int           PerPage { get; }

		public int GetNumPages() => ( this.Source.Count() - 1 ) / this.PerPage + 1;
		public async Task<int> GetNumPagesAsync() => ( await this.Source.CountAsync() - 1 ) / this.PerPage + 1;

		public IPageableViewQueryable<T> GetView( int curPage )
		{
			var skipping = ( curPage - 1 ) * this.PerPage;
			var taking   = Math.Min( this.PerPage, this.Source.Count() - skipping );
			return new PageableViewQueryable<T>( this.Source.Skip( skipping ).Take( taking ), skipping );
		}

		public async Task<IPageableViewQueryable<T>> GetViewAsync( int curPage )
		{
			var count    = await this.Source.CountAsync();
			var skipping = ( curPage - 1 ) * this.PerPage;
			var taking   = Math.Min( this.PerPage, count - skipping );
			return new PageableViewQueryable<T>( this.Source.Skip( skipping ).Take( taking ), skipping );
		}

		private sealed class PageableViewQueryable<TData> : IPageableViewQueryable<TData>
		{
			private readonly IQueryable<TData> inner;

			public PageableViewQueryable( IQueryable<TData> source, int startIndex )
			{
				this.inner      = source;
				this.StartIndex = startIndex;
			}

			public int StartIndex { get; }

			public IEnumerable<(int, TData)> WithIndices()
			{
				int i = this.StartIndex;
				foreach ( TData item in this.inner )
					yield return ( i++, item );
			}

			public IEnumerator<TData> GetEnumerator() => this.inner.GetEnumerator();
			IEnumerator IEnumerable.GetEnumerator() => ( (IEnumerable) this.inner ).GetEnumerator();
			public Type           ElementType => this.inner.ElementType;
			public Expression     Expression  => this.inner.Expression;
			public IQueryProvider Provider    => this.inner.Provider;

			public IAsyncEnumerator<TData> GetAsyncEnumerator( CancellationToken cancellationToken = new CancellationToken() )
				=> ( (IAsyncEnumerable<TData>) this.inner ).GetAsyncEnumerator( cancellationToken );
		}
	}

	public interface IPageableViewQueryable<T> : IQueryable<T>, IAsyncEnumerable<T>
	{
		IEnumerable<(int, T)> WithIndices();
		int StartIndex { get; }
	}
}