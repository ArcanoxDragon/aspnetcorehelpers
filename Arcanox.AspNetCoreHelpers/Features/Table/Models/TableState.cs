﻿namespace Arcanox.AspNetCoreHelpers.Features.Table.Models
{
	public class TableState
	{
		public string SortBy   { get; set; }
		public bool?  SortDesc { get; set; }
		public int?   Page     { get; set; }
		public string Search   { get; set; }
	}
}