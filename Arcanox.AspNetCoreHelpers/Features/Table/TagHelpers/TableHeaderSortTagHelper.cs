﻿using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Helpers;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Features.Table.TagHelpers
{
	[ HtmlTargetElement( "th", Attributes = "sort-by" ) ]
	public class TableHeaderSortTagHelper : TagHelper
	{
		private readonly IUrlHelperFactory      urlFactory;
		private readonly IActionContextAccessor actionContextAccessor;
		private readonly IHtmlHelper            html;

		public TableHeaderSortTagHelper( IUrlHelperFactory urlFactory,
										 IActionContextAccessor actionContextAccessor,
										 IHtmlHelper html )
		{
			this.urlFactory            = urlFactory;
			this.actionContextAccessor = actionContextAccessor;
			this.html                  = html;
		}

		[ HtmlAttributeName( "sort-by" ) ] public string SortBy { get; set; }

		[ HtmlAttributeNotBound ]
		[ ViewContext ]
		public ViewContext ViewContext { get; set; }

		public override async Task ProcessAsync( TagHelperContext context, TagHelperOutput output )
		{
			( (IViewContextAware) this.html ).Contextualize( this.ViewContext );

			var url            = this.urlFactory.GetUrlHelper( this.actionContextAccessor.ActionContext );
			var existingSort   = this.ViewContext.ViewData[ "SortBy" ]?.ToString();
			var sortDescending = this.ViewContext.ViewData.Get<bool>( "SortDescending" );
			var thContent      = await output.GetChildContentAsync();
			var sortBy         = Regex.Match( this.SortBy, @"\+?(.*)" ).Groups[ 1 ].Value;
			var sortDescFirst  = this.SortBy.StartsWith( "+" );
			var route = url.WithQueryParams( ( "sortBy", sortBy ),
											 ( "sortDesc", ( sortBy == existingSort && !sortDescending ) || ( sortBy != existingSort && sortDescFirst ) ) );
			var linkClasses = "";

			if ( sortBy == existingSort )
				linkClasses = sortDescending ? "sorted-desc" : "sorted-asc";

			var content = new HtmlContentBuilder()
						  .AppendHtml( $@"<a href=""{route}"" class=""{linkClasses}"">" )
						  .AppendHtml( thContent )
						  .AppendHtml( "</a>" );

			output.Content.SetHtmlContent( content );
		}
	}
}