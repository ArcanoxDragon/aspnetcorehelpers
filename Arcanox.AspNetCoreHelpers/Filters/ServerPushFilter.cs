﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Features.Resources.Abstract;
using Arcanox.AspNetCoreHelpers.Helpers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Primitives;

namespace Arcanox.AspNetCoreHelpers.Filters
{
	public class ServerPushFilter : IAsyncActionFilter
	{
		public async Task OnActionExecutionAsync( ActionExecutingContext context, ActionExecutionDelegate next )
		{
			context.HttpContext.Response.OnStarting( () => {
				lock ( context.HttpContext )
				{
					var urlProvider  = context.HttpContext.RequestServices.GetRequiredService<IResourceUrlProvider>();
					var linkPreloads = new List<string>();

					if ( context.HttpContext.Items[ ImportHelpers.KeyRequiredStyles ] is List<ImportHelpers.RequiredStyle> requiredStyles )
					{
						var stylePreloads = requiredStyles.Select( style => $"<{urlProvider.StyleUrl( style.Type, style.Name, true )}>; rel=preload; as=style" );

						linkPreloads.AddRange( stylePreloads );
					}

					if ( context.HttpContext.Items[ ImportHelpers.KeyRequiredScripts ] is List<ImportHelpers.RequiredScript> requiredScripts )
					{
						var scriptPreloads = requiredScripts.Select( script => $"<{urlProvider.ScriptUrl( script.Type, script.Name, true )}>; rel=preload; as=script" );

						linkPreloads.AddRange( scriptPreloads );
					}

					if ( linkPreloads.Any() )
					{
						var headers = context.HttpContext.Response.Headers;

						if ( headers.TryGetValue( "Link", out var linkValues ) )
						{
							linkPreloads.AddRange( linkValues );

							headers.Remove( "Link" );
						}

						headers.Add( "Link", new StringValues( linkPreloads.ToArray() ) );
					}
				}

				return Task.CompletedTask;
			} );

			await next();
		}
	}
}