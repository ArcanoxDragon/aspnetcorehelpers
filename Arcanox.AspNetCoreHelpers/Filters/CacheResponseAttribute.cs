﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Net.Http.Headers;

namespace Arcanox.AspNetCoreHelpers.Filters
{
	public class CacheResponseAttribute : ActionFilterAttribute
	{
		private const int DefaultMaxAgeSeconds = 0;
		private const int DefaultMaxAgeMinutes = 5;
		private const int DefaultMaxAgeHours   = 0;

		public int MaxAgeSeconds { get; set; }
		public int MaxAgeMinutes { get; set; }
		public int MaxAgeHours   { get; set; }

		public override Task OnResultExecutionAsync( ResultExecutingContext context, ResultExecutionDelegate next )
		{
			if ( this.MaxAgeSeconds + this.MaxAgeMinutes + this.MaxAgeHours == 0 )
			{
				this.MaxAgeSeconds = DefaultMaxAgeSeconds;
				this.MaxAgeMinutes = DefaultMaxAgeMinutes;
				this.MaxAgeHours   = DefaultMaxAgeHours;
			}

			context.HttpContext.Response.GetTypedHeaders().CacheControl = new CacheControlHeaderValue {
				Public = true,
				MaxAge = new TimeSpan( 0, this.MaxAgeHours, this.MaxAgeMinutes, this.MaxAgeSeconds )
			};

			return base.OnResultExecutionAsync( context, next );
		}
	}
}