﻿using System;

namespace Arcanox.AspNetCoreHelpers.Utilities
{
	public static class ActivatorHelper
	{
		public static bool TryCreateInstance<T>( out T instance )
		{
			try
			{
				instance = Activator.CreateInstance<T>();
				return true;
			}
			catch
			{
				instance = default;
				return false;
			}
		}
	}
}