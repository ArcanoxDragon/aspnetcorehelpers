﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Arcanox.AspNetCoreHelpers.Utilities.Linq
{
	class ExpressionHelper
	{
		public static LambdaExpression GetExpression<TModel>( string expressionText )
		{
			if ( string.IsNullOrEmpty( expressionText ) )
				throw new ArgumentNullException( nameof(expressionText) );

			var parameterExpression = Expression.Parameter( typeof( TModel ), "m" );
			var lambdaBody          = GetPropertyExpression( parameterExpression, expressionText );

			return Expression.Lambda( lambdaBody, parameterExpression );
		}

		public static Expression GetPropertyExpression( Expression modelExpression, string expressionText )
		{
			if ( string.IsNullOrEmpty( expressionText ) )
				throw new ArgumentNullException( nameof(expressionText) );

			string propertyName, remainder;

			if ( expressionText.Contains( "." ) )
			{
				remainder    = expressionText.Substring( expressionText.LastIndexOf( '.' ) + 1 );
				propertyName = expressionText.Substring( 0, expressionText.LastIndexOf( '.' ) );
			}
			else
			{
				propertyName = expressionText;
				remainder    = default;
			}

			var property = modelExpression.Type.GetProperty( propertyName, BindingFlags.Public | BindingFlags.Instance );

			if ( property == null )
				throw new InvalidOperationException( $"Invalid property name: {propertyName}" );

			var propertyExpression = Expression.Property( modelExpression, property );

			if ( remainder == default )
			{
				// End of recursion

				return propertyExpression;
			}

			return GetPropertyExpression( propertyExpression, remainder );
		}
	}
}