﻿using System;
using System.Linq;

namespace Arcanox.AspNetCoreHelpers.Utilities.Linq
{
	public interface ISearchProvider<T>
	{
		IQueryable<T> Search( IQueryable<T> source, string search );
	}

	[ AttributeUsage( AttributeTargets.Class, Inherited = false ) ]
	public class SearchProviderAttribute : Attribute { }
}