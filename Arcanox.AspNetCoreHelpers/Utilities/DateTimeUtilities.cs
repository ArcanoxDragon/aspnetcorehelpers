﻿using System;

namespace Arcanox.AspNetCoreHelpers.Utilities
{
	public static class DateTimeUtilities
	{
		public static readonly DateTime UnixEpoch = new DateTime( 1970, 1, 1, 0, 0, 0, DateTimeKind.Utc );
	}
}