﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Arcanox.AspNetCoreHelpers.Utilities
{
	public static class ReflectionHelper
	{
		private static IEnumerable<Assembly> GetAssemblies( string[] assemblies )
		{
			if ( assemblies == null || !assemblies.Any() )
				return AppDomain.CurrentDomain.GetAssemblies();

			return assemblies.Select( Assembly.Load );
		}

		public static Type[] GetTypesWithAttribute<TAttribute>( params string[] assemblies ) where TAttribute : Attribute
			=> ( from asm in GetAssemblies( assemblies )
				 from type in asm.GetTypes()
				 where type.GetCustomAttributes<TAttribute>().Any()
				 select type ).ToArray();

		public static Type[] GetTypesWithAttribute<TAttribute>( bool inherit, params string[] assemblies ) where TAttribute : Attribute
			=> ( from asm in GetAssemblies( assemblies )
				 from type in asm.GetTypes()
				 where type.GetCustomAttributes<TAttribute>( inherit ).Any()
				 select type ).ToArray();
	}
}