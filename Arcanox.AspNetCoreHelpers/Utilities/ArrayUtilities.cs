﻿namespace Arcanox.AspNetCoreHelpers.Utilities
{
    public static class ArrayUtilities
    {
		public static void Fill<T>( T[] array, T value )
		{
			for ( var i = 0; i < array.Length; i++ )
			{
				array[ i ] = value;
			}
		}
    }
}
