﻿using Newtonsoft.Json;

namespace Arcanox.AspNetCoreHelpers.Utilities.Json
{
	public static class JsonUtilities
	{
		public static bool TryDeserializeObject<T>( string value, out T model )
		{
			try
			{
				model = JsonConvert.DeserializeObject<T>( value );
				return true;
			}
			catch
			{
				// ignored
			}

			model = default;
			return false;
		}
	}
}