﻿using Microsoft.Extensions.FileProviders;

namespace Arcanox.AspNetCoreHelpers
{
    public static class AspNetCoreHelpers
    {
		public static IFileProvider GetEmbeddedFileProvider()
		{
			return new ManifestEmbeddedFileProvider( typeof( AspNetCoreHelpers ).Assembly );
		}
    }
}
