﻿using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Helpers.TagHelpers
{
	[ HtmlTargetElement( "script", Attributes = "[src]" ) ]
	public class ScriptVersionTagHelper : TagHelper
	{
		private readonly IHostingEnvironment environment;

		public ScriptVersionTagHelper( IHostingEnvironment environment )
		{
			this.environment = environment;
		}

		public override void Process( TagHelperContext context, TagHelperOutput output )
		{
			var srcAttribute = context.AllAttributes[ "src" ].Value?.ToString();

			if ( string.IsNullOrEmpty( srcAttribute ) )
				return;

			var srcMatch = Regex.Match( srcAttribute, @"^~/(.*\.js)", RegexOptions.IgnoreCase );

			if ( !srcMatch.Success )
				return;

			// Get just the filename (no query/tilde/etc)
			var fileName = srcMatch.Groups[ 1 ].Value;
			var webRoot  = this.environment.WebRootFileProvider;
			var fileInfo = webRoot.GetFileInfo( fileName );

			if ( fileInfo?.Exists != true )
				return;

			if ( srcAttribute.Contains( "?" ) )
				srcAttribute += $"&v={fileInfo.LastModified.ToFileTime()}";
			else
				srcAttribute += $"?v={fileInfo.LastModified.ToFileTime()}";

			output.Attributes.SetAttribute( "src", srcAttribute.Substring( 1 ) /* Remove tilde */ );
		}
	}
}