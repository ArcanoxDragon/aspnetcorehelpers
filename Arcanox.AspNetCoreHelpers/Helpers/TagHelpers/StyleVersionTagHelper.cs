﻿using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Helpers.TagHelpers
{
	[ HtmlTargetElement( "link", Attributes = "[rel=stylesheet],[href]" ) ]
	public class StyleVersionTagHelper : TagHelper
	{
		private readonly IHostingEnvironment environment;

		public StyleVersionTagHelper( IHostingEnvironment environment )
		{
			this.environment = environment;
		}

		public override void Process( TagHelperContext context, TagHelperOutput output )
		{
			var hrefAttribute = context.AllAttributes[ "href" ].Value?.ToString();

			if ( string.IsNullOrEmpty( hrefAttribute ) )
				return;

			var hrefMatch = Regex.Match( hrefAttribute, @"^~/(.*\.css)", RegexOptions.IgnoreCase );

			if ( !hrefMatch.Success )
				return;

			// Get just the filename (no query/tilde/etc)
			var fileName = hrefMatch.Groups[ 1 ].Value;
			var webRoot  = this.environment.WebRootFileProvider;
			var fileInfo = webRoot.GetFileInfo( fileName );

			if ( fileInfo?.Exists != true )
				return;

			if ( hrefAttribute.Contains( "?" ) )
				hrefAttribute += $"&v={fileInfo.LastModified.ToFileTime()}";
			else
				hrefAttribute += $"?v={fileInfo.LastModified.ToFileTime()}";

			output.Attributes.SetAttribute( "href", hrefAttribute.Substring( 1 ) /* Remove tilde */ );
		}
	}
}