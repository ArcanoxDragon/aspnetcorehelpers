﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Utilities;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Arcanox.AspNetCoreHelpers.Helpers
{
	public class RouteCollection : Dictionary<string, ScriptRoute>
	{
		public RouteCollection() { }
		public RouteCollection( IDictionary<string, ScriptRoute> source ) : base( source ) { }

		[ Obsolete( "Use Url.ScriptRoute instead" ) ]
		public void Add( string key, string url )
		{
			this.Add( key, new ScriptRoute { LegacyUrl = url } );
		}
	}

	public enum ScriptParamType
	{
		None,
		Number,
		Bool,
		DateTime,
		Guid,
		Alphanumeric
	}

	public class ScriptRoute
	{
		public string          Action     { get; set; }
		public string          Controller { get; set; }
		public string          Area       { get; set; }
		public string          ParamName  { get; set; }
		public ScriptParamType ParamType  { get; set; } = ScriptParamType.None;
		public object          RouteData  { get; set; }

		public string LegacyUrl { get; set; }

		public ScriptRoute WithParameter( string name, ScriptParamType type = ScriptParamType.None )
		{
			this.ParamName = name;
			this.ParamType = type;

			return this;
		}
	}

	public static class ScriptHelpers
	{
		[ Obsolete( "Use IUrlHelper.ScriptRoute" ) ]
		public static string ScriptAction( this IUrlHelper url,
										   [ AspMvcAction ] string action,
										   [ AspMvcController ] string controller,
										   [ AspMvcArea ] string area = "",
										   string paramName = null,
										   object routeData = null )
		{
			var routeValues = new RouteValueDictionary( routeData ?? new object() );

			if ( area != null )
				routeValues.Add( "area", area );

			if ( !string.IsNullOrEmpty( paramName ) )
				routeValues.Add( paramName, -9999 ); // -9999 is our flag for script-provided route parameters

			return url.Action( action, controller, routeValues );
		}

		public static ScriptRoute ScriptRoute( this IUrlHelper url,
											   [ AspMvcAction ] string action,
											   [ AspMvcController ] string controller,
											   [ AspMvcArea ] string area = "",
											   object routeData = null )
			=> new ScriptRoute {
				Action     = action,
				Controller = controller,
				Area       = area,
				RouteData  = routeData,
			};

		public static IHtmlContent ScriptRoutes( this IHtmlHelper html, RouteCollection routes )
		{
			var linkGenerator = html.ViewContext.HttpContext.RequestServices.GetRequiredService<LinkGenerator>();
			var builder = new HtmlContentBuilder()
						  .AppendHtmlLine( @"<script type=""text/javascript"">" )
						  .AppendHtmlLine( @"var Routes = window.Routes || ( window.Routes = {} );" );

			foreach ( var (key, route) in routes )
			{
				var routeUrl = default( string );

				if ( !route.LegacyUrl.IsNullOrEmpty() )
				{
					routeUrl = route.LegacyUrl;

					if ( routeUrl.Contains( "-9999" ) )
					{
						// Replace legacy parameter with JS snippet to retrieve value from function argument
						routeUrl = routeUrl.Replace( "-9999", "' + p + '" );
					}
				}
				else
				{
					var routeValues  = new RouteValueDictionary( route.RouteData ?? new object() );
					var hasParameter = !route.ParamName.IsNullOrEmpty();
					var parameterKey = default( string );

					if ( !route.Area.IsNullOrEmpty() )
						routeValues.Add( "area", route.Area );

					if ( hasParameter )
					{
						// Have to generate the most specific key we can that still fits in the parameter's type constraint
						switch ( route.ParamType )
						{
							case ScriptParamType.Bool:
								parameterKey = "FaLsE";
								break;
							case ScriptParamType.DateTime:
								parameterKey = $"{DateTimeUtilities.UnixEpoch:yyyy-MM-dd}";
								break;
							case ScriptParamType.Alphanumeric:
								parameterKey = $"parameter{Regex.Replace( route.ParamName, @"[^a-zA-Z]", "" )}";
								break;
							case ScriptParamType.Guid:
								parameterKey = $"{Guid.NewGuid():N}";
								break;
							case ScriptParamType.Number:
								parameterKey = $"{int.MinValue}";
								break;
							default:
								parameterKey = $"__PARAMETER_{route.ParamName}__";
								break;
						}

						// Put a URL-safe token representing the parameter name in the URL
						routeValues.Add( route.ParamName, parameterKey );
					}

					// Generate URL and replace any potential single quotes (of which there really shouldn't be any) with double quotes
					routeUrl = linkGenerator.GetPathByAction( route.Action, route.Controller, routeValues )?.Replace( '\'', '"' );

					if ( string.IsNullOrEmpty( routeUrl ) )
					{
						// Couldn't generate the URL for some reason
						var values = routeValues.Keys
												.Select( k => $"{k} = {routeValues[ k ]}" )
												.Concat( new[] {
													$"controller = {route.Controller}",
													$"action = {route.Action}",
												} )
												.Join( "; " );

						throw new InvalidOperationException( $"Could not find a route matching the provided route values: {values}" );
					}

					if ( hasParameter )
					{
						// Now that the URL has been generated, replace the parameter token with JS snippet to retrieve value from function argument
						routeUrl = routeUrl.Replace( parameterKey, "' + p + '" );
					}
				}

				builder = builder.AppendHtmlLine( $@"Routes[""{key}""] = function(p) {{ return '{routeUrl}'; }}" );
			}

			return builder.AppendHtmlLine( @"</script>" );
		}

		public static IHtmlContent ScriptRoutes( this IHtmlHelper html, params (string key, ScriptRoute route)[] routes )
			=> html.ScriptRoutes( new RouteCollection( routes.ToDictionary( route => route.key, route => route.route ) ) );

		public static IHtmlContent ScriptData( this IHtmlHelper html, object data )
		{
			data.EnsureNotNull( nameof(data) );

			return new HtmlContentBuilder()
				   .AppendHtmlLine( @"<script type=""text/javascript"">" )
				   .AppendHtmlLine( @"var PageData = window.PageData || ( window.PageData = {} );" )
				   .AppendHtmlLine( $@"Object.assign( PageData, JSON.parse(`{JsonConvert.SerializeObject( data )}`) );" )
				   .AppendHtmlLine( @"</script>" );
		}
	}
}