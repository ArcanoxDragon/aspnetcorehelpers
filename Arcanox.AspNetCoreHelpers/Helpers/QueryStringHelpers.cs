﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Arcanox.AspNetCoreHelpers.Helpers
{
	public static class QueryStringHelpers
	{
		public static string WithQueryParam( this IUrlHelper url, ICollection<string> excludeParams, string param, object value ) => url.WithQueryParams( excludeParams, (param, value) );

		public static string WithQueryParam( this IUrlHelper url, string param, object value ) => url.WithQueryParam( null, param, value );

		public static string WithQueryParams( this IUrlHelper url, ICollection<string> excludeParams, params (string param, object value)[] queryParams )
		{
			var request = url.ActionContext.HttpContext.Request;
			var query = request.Query;
			var queryDict = query.ToDictionary( p => p.Key, p => p.Value.ToArray() );

			foreach ( var (param, value) in queryParams )
			{
				if ( value == null )
					queryDict.Remove( param );
				else
					queryDict[ param ] = new[] { value.ToString() };
			}

			if ( excludeParams?.Any() == true )
				foreach ( var exclude in excludeParams )
					queryDict.Remove( exclude );

			var newQuery = new QueryString();

			foreach ( var key in queryDict.Keys )
			foreach ( var val in queryDict[ key ] )
				newQuery = newQuery.Add( key, val );

			return request.Path.Add( newQuery );
		}

		public static string WithQueryParams( this IUrlHelper url, params (string param, object value)[] queryParams ) => url.WithQueryParams( null, queryParams );
	}
}