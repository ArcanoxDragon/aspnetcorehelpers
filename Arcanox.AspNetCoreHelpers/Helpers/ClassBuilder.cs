﻿using System.Collections;
using System.Collections.Generic;

namespace Arcanox.AspNetCoreHelpers.Helpers
{
	public class ClassBuilder : IEnumerable<string>
	{
		private readonly List<string> classes;

		public ClassBuilder()
		{
			this.classes = new List<string>();
		}

		public ClassBuilder Add( string className )
			=> this.Add( className, true );

		public ClassBuilder Add( string className, bool classEnabled )
		{
			if ( classEnabled )
				this.classes.Add( className );

			return this;
		}

		public string Build() => string.Join( " ", this.classes );

		#region Implementation of IEnumerable

		public IEnumerator<string> GetEnumerator() => this.classes.GetEnumerator();
		IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

		#endregion
	}
}