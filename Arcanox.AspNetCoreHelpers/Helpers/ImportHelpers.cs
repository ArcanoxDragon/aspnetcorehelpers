﻿using System.Collections.Generic;
using System.Linq;
using Arcanox.AspNetCoreHelpers.Extensions;
using Arcanox.AspNetCoreHelpers.Features.Resources;
using Arcanox.AspNetCoreHelpers.Features.Resources.Abstract;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;

namespace Arcanox.AspNetCoreHelpers.Helpers
{
	public static class ImportHelpers
	{
		internal const string KeyRequiredScripts = "RequiredScripts";
		internal const string KeyRequiredStyles  = "RequiredStyles";

		internal class RequiredResource
		{
			public ResourceType Type     { get; set; }
			public string       Id       { get; set; }
			public string       Name     { get; set; }
			public bool         SkipEmit { get; set; }
		}

		internal class RequiredStyle : RequiredResource { }

		internal class RequiredScript : RequiredResource
		{
			public bool IsAsync   { get; set; }
			public bool IsDefered { get; set; }
		}

		private static IResourceUrlProvider GetUrlProvider( IHtmlHelper html )
		{
			var services = html.ViewContext.HttpContext.RequestServices;

			return services.GetRequiredService<IResourceUrlProvider>();
		}

		private static string MakeScriptTag( IHtmlHelper html, ResourceType type, string scriptName, bool defer, bool async, string id )
		{
			var attrId    = string.IsNullOrEmpty( id ) ? "" : $"id=\"{id}\"";
			var attrSrc   = $"src=\"{GetUrlProvider( html ).ScriptUrl( type, scriptName, true )}\"";
			var attrDefer = defer ? "defer" : "";
			var attrAsync = async ? "async" : "";

			return $@"<script {attrId} {attrSrc} {attrDefer} {attrAsync}></script>";
		}

		private static string MakeStyleTag( IHtmlHelper html, ResourceType type, string styleName, string id )
		{
			var attrId   = string.IsNullOrEmpty( id ) ? "" : $"id=\"{id}\"";
			var attrHref = $"href=\"{GetUrlProvider( html ).StyleUrl( type, styleName, true )}\"";

			return $@"<link {attrId} rel=""stylesheet"" type=""text/css"" {attrHref} />";
		}

		private static IHtmlContent RequireScript( this IHtmlHelper html, ResourceType type, string scriptUrl, bool defer, bool async, bool immediate, string id )
		{
			lock ( html.ViewContext.HttpContext )
			{
				if ( !( html.ViewContext.HttpContext.Items[ KeyRequiredScripts ] is List<RequiredScript> required ) )
					html.ViewContext.HttpContext.Items[ KeyRequiredScripts ] = required = new List<RequiredScript>();

				if ( !required.Any( r => r.Name == scriptUrl ) )
					required.Add( new RequiredScript {
						Type      = type,
						Name      = scriptUrl,
						IsAsync   = async,
						IsDefered = defer,
						SkipEmit  = immediate,
						Id        = id,
					} );

				if ( immediate )
					// "immediate" flag causes script tag to immediately be emitted
					// (instead of emitted with the call to EmitRequiredScripts)
					// but still be added to the push list when using HTTP/2 push
					return html.Raw( MakeScriptTag( html, type, scriptUrl, defer, async, id ) );

				return null;
			}
		}

		public static IHtmlContent RequireLibScript( this IHtmlHelper html, string scriptName, bool defer = false, bool async = false, bool immediate = false, string id = null )
			=> html.RequireScript( ResourceType.Lib, scriptName, defer, async, immediate, id );

		public static IHtmlContent RequirePageScript( this IHtmlHelper html, string scriptName, bool defer = true, bool async = true, bool immediate = false, string id = null )
			=> html.RequireScript( ResourceType.Page, scriptName, defer, async, immediate, id );

		public static IHtmlContent RequirePageScript( this IHtmlHelper html, string scriptName, RouteCollection routes, bool defer = true, bool async = true, bool immediate = true, string id = null )
			=> new HtmlContentBuilder()
			   .AppendHtml( html.ScriptRoutes( routes ) )
			   .AppendHtml( html.RequirePageScript( scriptName, defer, async, immediate, id ) );

		public static IHtmlContent RequirePageScript( this IHtmlHelper html, string scriptName, params (string key, ScriptRoute route)[] routes )
			=> html.RequirePageScript( scriptName, new RouteCollection( routes.ToDictionary( route => route.key, route => route.route ) ) );

		private static IHtmlContent RequireStyle( this IHtmlHelper html, ResourceType type, string styleUrl, bool immediate, string id )
		{
			lock ( html.ViewContext.HttpContext )
			{
				if ( !( html.ViewContext.HttpContext.Items[ KeyRequiredStyles ] is List<RequiredStyle> required ) )
					html.ViewContext.HttpContext.Items[ KeyRequiredStyles ] = required = new List<RequiredStyle>();

				if ( !required.Any( r => r.Name == styleUrl ) )
					required.Add( new RequiredStyle {
						Type     = type,
						Name     = styleUrl,
						SkipEmit = immediate,
						Id       = id,
					} );

				if ( immediate )
					// "immediate" flag causes script tag to immediately be emitted
					// (instead of emitted with the call to EmitRequiredScripts)
					// but still be added to the push list when using HTTP/2 push
					return html.Raw( MakeStyleTag( html, type, styleUrl, id ) );

				return null;
			}
		}

		public static IHtmlContent RequireLibStyle( this IHtmlHelper html, string styleName, bool immediate = false, string id = null )
			=> html.RequireStyle( ResourceType.Lib, styleName, immediate, id );

		public static IHtmlContent RequirePageStyle( this IHtmlHelper html, string styleName, bool immediate = false, string id = null )
			=> html.RequireStyle( ResourceType.Page, styleName, immediate, id );

		public static IHtmlContent EmitRequiredScripts( this IHtmlHelper html )
		{
			lock ( html.ViewContext.HttpContext )
			{
				IHtmlContentBuilder builder = new HtmlContentBuilder();

				if ( html.ViewContext.HttpContext.Items[ KeyRequiredScripts ] is List<RequiredScript> requiredScripts )
				{
					builder = requiredScripts.Where( s => !s.SkipEmit )
											 .AsReverse()
											 .Aggregate(
												 builder,
												 ( current, script ) => current.AppendHtmlLine(
													 MakeScriptTag( html, script.Type, script.Name, script.IsDefered, script.IsAsync, script.Id )
												 )
											 );
				}

				return builder;
			}
		}

		public static IHtmlContent EmitRequiredStyles( this IHtmlHelper html )
		{
			lock ( html.ViewContext.HttpContext )
			{
				IHtmlContentBuilder builder = new HtmlContentBuilder();

				if ( html.ViewContext.HttpContext.Items[ KeyRequiredStyles ] is List<RequiredStyle> requiredStyles )
				{
					builder = requiredStyles.Where( s => !s.SkipEmit )
											.AsReverse()
											.Aggregate(
												builder,
												( current, style ) => current.AppendHtmlLine(
													MakeStyleTag( html, style.Type, style.Name, style.Id )
												)
											);
				}

				return builder;
			}
		}
	}
}