declare interface JQuerySimpleWebSocketOptions {
    url: string;
    protocols?: string;
    timeout?: number;
    attempts?: number;
    dataType?: "xml" | "text" | "json";
}

declare interface JQuerySimpleWebSocket {
    connect();
    isConnected(): boolean;
    isConnected( callback: ( connected: boolean ) => void );
    send( data: any );
    listen( callback: ( data: any ) => void );
    remove( callback: ( data: any ) => void );
    removeAll();
    close();
}

declare interface JQueryStatic {
    simpleWebSocket( opts: JQuerySimpleWebSocketOptions ): JQuerySimpleWebSocket;
}