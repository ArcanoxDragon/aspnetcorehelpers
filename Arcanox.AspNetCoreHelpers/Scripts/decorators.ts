export function BindMethod( _: any, propertyName: string, descriptor: PropertyDescriptor ) {
    let unboundFunc = descriptor.value;

    return {
        configurable: true,

        get() {
            let boundFunc = unboundFunc.bind( this );

            // Define instance property so we don't have to re-bind it every time the function is accessed
            Reflect.defineProperty( this, propertyName, {
                value: boundFunc,
                configurable: true,
                writable: true
            } );

            return boundFunc;
        }
    } as PropertyDescriptor;
}