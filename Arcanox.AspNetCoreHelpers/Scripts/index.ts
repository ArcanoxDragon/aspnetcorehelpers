import * as Ajax from "./ajax";
import * as Events from "./domEvents";
import * as Flashes from "./flashes";
import * as Forms from "./forms";
import * as ReactHelpers from "./ReactHelpers";

import WebSockets from "./webSockets";

export * from "./decorators";

type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
type Subtract<T extends K, K> = Omit<T, keyof K>;
type Route = ( ( param?: any ) => string );

export {
	Ajax,
	Events,
	Flashes,
	Forms,
	ReactHelpers,
	WebSockets,
	Omit,
	Subtract,
	Route
};

declare global {
	export const Routes: { [ name: string ]: Route };
	export const PageData: { [ name: string ]: any };
}