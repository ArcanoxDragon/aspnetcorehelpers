export function displayFlashes() {
    // flash messages (which are turned into toasts)
    $( "div#flashes" )
        .find( ".flashBox" )
        .each( function() {
            let $flash = $( this );
            let toastrFunc = toastr.info;

            if ( $flash.hasClass( "success" ) )
                toastrFunc = toastr.success;
            if ( $flash.hasClass( "error" ) )
                toastrFunc = toastr.error;

            let $toast = toastrFunc( $flash.html() );

            if ( $flash.data( "flashdata" ) ) {
                $toast.data( "flashdata", $flash.data( "flashdata" ) );
            }
        } )
        .remove();
}