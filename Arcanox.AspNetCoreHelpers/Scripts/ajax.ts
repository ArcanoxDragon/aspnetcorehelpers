﻿export async function execute<TResult = void>( options: JQueryAjaxSettings ): Promise<[ boolean, TResult, any ]> {
    try {
        let result = await $.ajax( options );

        return [ true, result as TResult, undefined ];
    } catch ( err ) {
        return [ false, undefined, err ];
    }
}

export default {
    execute
}