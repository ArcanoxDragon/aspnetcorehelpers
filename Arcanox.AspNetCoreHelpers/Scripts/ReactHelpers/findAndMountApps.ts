﻿import React from "react";
import ReactDOM from "react-dom";

export interface AppResolver {
	( appName: string ): Promise<React.ComponentType>;
}

async function mountAppToTag( $tag: HTMLElement, resolver: AppResolver ) {
	let appName = $tag.dataset.appName;
	let propsJson = $tag.dataset.props;
	let props: object = {};

	if ( !appName ) {
		console.error( "App name not specified for React element:" );
		console.error( $tag );
		return;
	}

	if ( propsJson ) {
		try {
			props = JSON.parse( propsJson );

			if ( !props ) {
				throw new Error( "Could not parse props" );
			}
		} catch ( err ) {
			console.error( `Error mouting React app \"${appName}\":` );
			console.error( err );
			return;
		}
	}

	let appComponent = await resolver( appName );

	if ( typeof appComponent[ "appClass" ] === "string" ) {
		$tag.classList.add( appComponent[ "appClass" ] );
	}

	ReactDOM.render( React.createElement( appComponent, props ), $tag );
}

export default function findAndMountApps( resolver: AppResolver ) {
	let reactElements = Array.from( document.querySelectorAll( "[data-app-name]" ) );

	reactElements.forEach( ( el: HTMLElement ) => mountAppToTag( el, resolver ) );
}