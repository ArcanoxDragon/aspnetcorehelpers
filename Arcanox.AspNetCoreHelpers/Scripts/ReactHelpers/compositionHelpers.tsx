﻿import { Context, ComponentType } from "react";
import { fromRenderProps } from "recompose";

export function createContextComposer<TContext, TContextProps>(
    context: Context<TContext>,
    buildProps: ( context: TContext ) => TContextProps
) {
    return function<TProps>( component: ComponentType<TProps & TContextProps> ) {
        return fromRenderProps<TContextProps, TProps, TContext>( context.Consumer, buildProps )( component );
    }
}