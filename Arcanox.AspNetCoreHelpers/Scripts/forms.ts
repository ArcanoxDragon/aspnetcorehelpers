﻿export function markInputDirty( input: JQuery | HTMLElement ) {
    let $i = $( input as HTMLElement );
    let $window = $( window );

    if ( !$i.data( "dirty-hooked" ) ) {
        $i.data( "dirty-hooked", true );
        let windowHookedInputs = +( $window.data( "dirty-hooked-count" ) || 0 );
        if ( windowHookedInputs === 0 ) {
            $window.on( "beforeunload", () => {
                return "Are you sure you want to leave the page? Unsaved changes will be lost.";
            } );
        }
        $window.data( "dirty-hooked-count", windowHookedInputs + 1 );
    }
}

export function markInputClean( input: JQuery | HTMLElement ) {
    let $i = $( input as HTMLElement );
    let $window = $( window );

    if ( !!$i.data( "dirty-hooked" ) ) {
        $i.data( "dirty-hooked", false );
        let windowHookedInputs = +( $window.data( "dirty-hooked-count" ) || 0 );
        if ( windowHookedInputs === 1 ) {
            $window.off( "beforeunload" );
        }
        $window.data( "dirty-hooked-count", windowHookedInputs - 1 );
    }
}

export function hookDirtyWarning( input: JQuery | HTMLElement ) {
    let $i = $( input as HTMLElement );
    let origValue = $i.val();

    function handler() {
        let curValue = $i.val();

        if ( curValue === origValue ) {
            markInputClean( $i );
        } else {
            markInputDirty( $i );
        }
    }

    $i.on( "keyup change", handler ).data( "dirty-handler", handler );

    return ( newCleanVal: string ) => {
        origValue = newCleanVal;
    };
}

export function clearDirtyWarningHook( input: JQuery | HTMLElement ) {
    let $i = $( input as HTMLElement );
    let handler = $i.data( "dirty-handler" );

    markInputClean( $i );

    if ( !handler )
        return;

    $i.off( "keyup change", handler );
}

export function postForm( url: string, formData?: any ) {
    let $form = $( "<form>" ).attr( { action: url, method: "POST" } );

    function valueToString( value: any ) {
        if ( [ "string", "number", "boolean" ].indexOf( typeof value ) >= 0 ) {
            return value.toString();
        } else {
            return JSON.stringify( value );
        }
    }

    function appendItem( key: string, item: any ) {
        let name = key;
        let value = valueToString( item );
        let $input = $( "<input type='hidden'>" ).attr( { name, value } );

        $form.append( $input );
    }

    if ( formData && typeof formData === "object" ) {
        for ( let key of Object.keys( formData ) ) {
            let value = formData[ key ];

            if ( Array.isArray( value ) ) {
                value.forEach( ( item, index ) => {
                    let name = `${key}[${index}]`;

                    appendItem( name, item );
                } );
            } else {
                appendItem( key, value );
            }
        }
    }

    $form.appendTo( "body" ).submit();
}

export default {
    markInputDirty,
    markInputClean,
    hookDirtyWarning,
    clearDirtyWarningHook,
    postForm
};