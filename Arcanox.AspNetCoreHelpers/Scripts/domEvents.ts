export type KeyboardEventHandler<TElement extends HTMLElement = HTMLElement>
    = ( event: React.KeyboardEvent<TElement> ) => void;

export interface SpecialKeyboardEvents<TElement extends HTMLElement = HTMLElement> {
    escape?: KeyboardEventHandler<TElement>;
    enter?: KeyboardEventHandler<TElement>;
    default?: KeyboardEventHandler<TElement>;
    preventDefault?: boolean;
}

export const KeyEscape = 27;
export const KeyEnter = 13;

export function getSpecialKeyupHandler<TElement extends HTMLElement = HTMLElement>(
    events: SpecialKeyboardEvents<TElement>
): KeyboardEventHandler<TElement> {
    // Default to true unless it's explicitly set false
    let preventDefault = events.preventDefault === false ? false : true;

    return ( e: React.KeyboardEvent<TElement> ) => {
        if ( e.keyCode === KeyEscape && events.escape ) {
            if ( preventDefault ) {
                e.preventDefault();
            }

            events.escape.call( undefined, e );
        } else if ( e.keyCode === KeyEnter && events.enter ) {
            if ( preventDefault ) {
                e.preventDefault();
            }

            events.enter.call( undefined, e );
        } else if ( events.default ) {
            events.default.call( undefined, e );
        }
    };
}

export default {
    KeyEscape,
    KeyEnter,

    getSpecialKeyupHandler,
};