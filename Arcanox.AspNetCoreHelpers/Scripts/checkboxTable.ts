export const RowCheckboxClassName = "row-checkbox";

const RowCheckboxSelector = `input[type=checkbox].${RowCheckboxClassName}`;

export function initCheckboxTable( $table: JQuery ) {
    if ( !$table.is( "table" ) ) {
        console.warn( "Warning: Non-table element passed to initCheckboxTable. This function only works on table elements." );
        return;
    }

    let $tbody = $table.find( "tbody" );

    if ( $tbody.length === 0 ) {
        console.warn( "Warning: the table provided to initCheckboxTable does not contain a tbody tag. All tables must have a tbody tag." );
        return;
    }

    // Populate first-column checkboxes with identifier class
    $tbody.find( "tr" ).each( ( i, row ) => {
        $( row ).find( "td" ).first().find( "input[type=checkbox]" ).addClass( RowCheckboxClassName );
    } );

    $( "body" ).click( e => {
        let fromTable = $.contains( $table.get( 0 ), e.target );
        let onLink = $( e.target ).is( "a" ) || $( e.target ).parents( "a" ).length > 0;

        if ( !fromTable && !onLink ) {
            $tbody.find( RowCheckboxSelector ).prop( "checked", false ).trigger( "change" );
        }
    } );

    let $checkAll = $table.find( ".check-all" );
    let lastRowClicked = null;

    $checkAll.change( () => {
        let checked = $checkAll.is( ":checked" );

        $table.find( RowCheckboxSelector ).prop( { checked } );
        $tbody.find( "tr" ).toggleClass( "selected", checked );
    } );

    $tbody.find( ".checkbox" )
        .mousedown( e => e.stopPropagation() );

    $tbody.find( RowCheckboxSelector )
        .change( function () {
            let $check = $( this );

            // Update row class
            $check.parents( "tr" ).first().toggleClass( "selected", $check.is( ":checked" ) );

            // Update "select all" checkbox
            let allChecked = $tbody.find( `${RowCheckboxSelector}:not(:checked)` ).length === 0;

            $checkAll.prop( "checked", allChecked );
        } );

    $tbody.find( "tr" ).mousedown( function ( e ) {
        e.stopPropagation();

        let $tr = $( this );

        if ( e.ctrlKey ) {
            $tr.find( RowCheckboxSelector ).click();
        } else if ( lastRowClicked && e.shiftKey ) {
            // prevent text selection
            e.preventDefault();

            let rows = $tbody.find( "tr" );
            let thisIndex = rows.index( $tr );
            let lastClickedIndex = rows.index( lastRowClicked );
            let from: number, to: number;

            if ( thisIndex > lastClickedIndex ) {
                [ from, to ] = [ lastClickedIndex, thisIndex ];
            } else {
                [ from, to ] = [ thisIndex, lastClickedIndex ];
            }

            rows.find( RowCheckboxSelector ).prop( "checked", false ).trigger( "change" );
            rows.slice( from, to + 1 ).find( RowCheckboxSelector ).prop( "checked", true ).trigger( "change" );
        } else {
            $tbody.find( RowCheckboxSelector ).prop( "checked", false ).trigger( "change" );
            $tr.find( RowCheckboxSelector ).prop( "checked", true ).trigger( "change" );
        }

        lastRowClicked = $tr;
    } );
}

export function getCheckedRows( $table: JQuery ) {
    if ( !$table.is( "table" ) ) {
        console.warn( "Warning: Non-table element passed to getCheckedRows. This function only works on table elements." );
        return;
    }

    let $tbody = $table.find( "tbody" );

    if ( $tbody.length === 0 ) {
        console.warn( "Warning: the table provided to initCheckboxTable does not contain a tbody tag. All tables must have a tbody tag." );
        return;
    }

    let rows = $tbody.find( "tr" );

    return rows.filter( ( i, row ) => $( row ).find( `${RowCheckboxSelector}:checked` ).length > 0 );
}