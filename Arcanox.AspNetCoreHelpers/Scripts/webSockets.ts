import { EventEmitter } from "events";
import { BindMethod } from "./decorators";

export interface WebSocketMessage {
    type: string;
    data?: any;
}

interface WebSockets {
    on( messageType: string, listener: ( data: any ) => void );
}

class WebSockets extends EventEmitter {
    private socket: JQuerySimpleWebSocket;

    init( path: string = "/ws" ) {
        let protocol = ( window.location.protocol === "https:" ) ? "wss" : "ws";

        this.socket = $.simpleWebSocket( {
            url: `${protocol}://${window.location.host}${path}`
        } );
        this.socket.listen( this.onData );

        setInterval( this.keepalive, 30 * 1000 /* 30 seconds keepalive */ );
    }

    @BindMethod
    private onData( message: WebSocketMessage ) {
        if ( !message || typeof message.type !== "string" ) {
            return;
        }

        this.emit( message.type, message.data );
    }

    @BindMethod
    private keepalive() {
        this.socket.send( { type: "keepalive" } );
    }
}

export default new WebSockets();