﻿using System;

namespace Arcanox.AspNetCoreHelpers.Attributes
{
	public abstract class ServiceAttribute : Attribute
	{
		protected ServiceAttribute( Type contractType = null )
		{
			this.ContractType = contractType;
		}

		public Type ContractType { get; set; }
		public int  Priority     { get; set; } = 0;
	}

	[ AttributeUsage( AttributeTargets.Class, AllowMultiple = true, Inherited = false ) ]
	public class TransientAttribute : ServiceAttribute
	{
		public TransientAttribute( Type contractType = null ) : base( contractType ) { }
	}

	[ AttributeUsage( AttributeTargets.Class, AllowMultiple = true, Inherited = false ) ]
	public class ScopedAttribute : ServiceAttribute
	{
		public ScopedAttribute( Type contractType = null ) : base( contractType ) { }
	}

	[ AttributeUsage( AttributeTargets.Class, AllowMultiple = true, Inherited = false ) ]
	public class SingletonAttribute : ServiceAttribute
	{
		public SingletonAttribute( Type contractType = null ) : base( contractType ) { }
	}
}