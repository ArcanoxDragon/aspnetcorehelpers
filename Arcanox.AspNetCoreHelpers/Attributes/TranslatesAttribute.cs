﻿using System;

namespace Arcanox.AspNetCoreHelpers.Attributes
{
	public class TranslatesAttribute : Attribute
	{
		public TranslatesAttribute( Type declaringType, string methodName )
		{
			this.DeclaringType  = declaringType;
			this.MethodName     = methodName;
		}

		public Type   DeclaringType  { get; }
		public string MethodName     { get; }
	}
}