﻿using System;

namespace Arcanox.AspNetCoreHelpers.Attributes
{
	/// <inheritdoc />
	/// <summary>
	/// Indicates that a class represents a database model.
	/// The class must have a public static method with the following name and signature in order to be registered:
	/// <para />
	/// public static void Register( EntityTypeBuilder&lt;TModel&gt; entity )
	/// </summary>
	public class ModelAttribute : Attribute { }
}