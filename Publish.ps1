$key = Get-Content NuGetKey.txt
$source = "https://nuget.arcanox.me/v3/index.json"

if ( Test-Path NuGet ) {
    Remove-Item -Force -Recurse NuGet
}

New-Item -ItemType Directory -Name NuGet
Get-ChildItem -Recurse | 
    Where-Object { $_.DirectoryName -match "bin" -and $_.Name -match "\.nupkg$" } | 
    ForEach-Object {
        Copy-Item -Destination NuGet $($_.FullName)
        Remove-Item $($_.FullName)
    }
    
Get-ChildItem -Path NuGet -Filter *.nupkg | ForEach-Object {
    Write-Host "Publishing package $($_.FullName) to $source..."
    
    nuget push "$($_.FullName)" -Source $source -ApiKey $key
    Remove-Item $($_.FullName)
}