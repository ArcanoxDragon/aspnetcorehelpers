﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arcanox.AspNetCoreHelpers.Features.Table.Extensions;
using Arcanox.AspNetCoreHelpers.Features.Table.Models.ViewModels;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Arcanox.AspNetCoreHelpers.Bootstrap.Helpers
{
	public static class BootstrapHelper
	{
		public static IHtmlContent Paginator( this IHtmlHelper html, int currentPage, int totalPages, IEnumerable<string> excludeQueryParams = null )
		{
			var excludeQueryParamsArray = excludeQueryParams?.ToArray() ?? new string[ 0 ];

			#pragma warning disable MVC1000 // Don't warn on an API method
			return html.Partial( "Partials/_Paginator", new PaginatorViewModel {
				ExcludeQueryParams = excludeQueryParamsArray,
				CurrentPage        = currentPage,
				TotalPages         = totalPages
			} );
			#pragma warning restore MVC1000
		}

		public static IHtmlContent Paginator<TModel>( this IHtmlHelper html, TableViewModel<TModel> model, IEnumerable<string> excludeQueryParams = null )
		{
			var dataSet  = model.ToDataSet();
			var numPages = dataSet.GetNumPages();

			return html.Paginator( model.CurPage, numPages, excludeQueryParams );
		}

		public static async Task<IHtmlContent> PaginatorAsync<TModel>( this IHtmlHelper html, TableViewModel<TModel> model, IEnumerable<string> excludeQueryParams = null )
		{
			var dataSet  = model.ToDataSet();
			var numPages = await dataSet.GetNumPagesAsync();

			return html.Paginator( model.CurPage, numPages, excludeQueryParams );
		}
	}
}