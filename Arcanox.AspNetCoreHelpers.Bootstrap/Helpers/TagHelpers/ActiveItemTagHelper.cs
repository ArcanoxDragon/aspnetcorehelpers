﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Bootstrap.Helpers.TagHelpers
{
	[ HtmlTargetElement( Attributes = "[active-on], [active-value]" ) ]
	public class ActiveItemTagHelper : TagHelper
	{
		public string ActiveOn    { get; set; }
		public string ActiveValue { get; set; }

		[ ViewContext ]
		public ViewContext ViewContext { get; set; }

		[ HtmlAttributeName( "active-exact" ) ]
		public bool Exact { get; set; }

		public override void Process( TagHelperContext context, TagHelperOutput output )
		{
			var currentValue = this.ViewContext.ViewData[ this.ActiveOn ]?.ToString();
			var matches      = false;

			if ( this.Exact && currentValue == this.ActiveValue )
			{
				matches = true;
			}
			else
			{
				if ( !string.IsNullOrEmpty( currentValue ) )
				{
					var values = currentValue.Split( ':' ).Where( s => !string.IsNullOrEmpty( s ) );

					if ( values.Any( v => v == this.ActiveValue ) )
						matches = true;
				}
			}

			if ( matches )
			{
				// Add "active" class
				var existingClass = "";

				if ( output.Attributes.Any( a => a.Name.ToLower() == "class" ) )
				{
					existingClass = output.Attributes[ "class" ].Value.ToString();

					output.Attributes.RemoveAll( "class" );
				}

				output.Attributes.Add( "class", $"{existingClass} active" );
			}
		}
	}
}