﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Bootstrap.Helpers.TagHelpers
{
	[ HtmlTargetElement( "b-card" ) ]
	[ RestrictChildren( "b-card-header", "b-card-body", "b-card-footer" ) ]
	public class CardTagHelper : SimpleTagWithClassesTagHelper
	{
		public CardTagHelper() : base( "card" ) { }
	}

	[ HtmlTargetElement( "b-card-header" ) ]
	public class CardHeaderTagHelper : SimpleTagWithClassesTagHelper
	{
		public CardHeaderTagHelper() : base( "card-header" ) { }
	}

	[ HtmlTargetElement( "b-card-body" ) ]
	public class CardBodyTagHelper : SimpleTagWithClassesTagHelper
	{
		public CardBodyTagHelper() : base( "card-body" ) { }
	}

	[ HtmlTargetElement( "b-card-footer" ) ]
	public class CardFooterTagHelper : SimpleTagWithClassesTagHelper
	{
		public CardFooterTagHelper() : base( "card-footer" ) { }
	}
}