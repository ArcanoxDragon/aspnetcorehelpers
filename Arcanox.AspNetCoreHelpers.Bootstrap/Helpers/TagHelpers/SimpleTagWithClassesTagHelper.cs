﻿using Arcanox.AspNetCoreHelpers.Extensions;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Bootstrap.Helpers.TagHelpers
{
	public abstract class SimpleTagWithClassesTagHelper : TagHelper
	{
		protected SimpleTagWithClassesTagHelper( params string[] classes )
		{
			this.Classes = classes;
		}

		[ HtmlAttributeNotBound ]
		protected string TagName { get; set; } = "div";

		public string[] Classes { get; }

		public override void Process( TagHelperContext context, TagHelperOutput output )
		{
			output.TagName = this.TagName;
			output.Attributes.AppendClasses( this.Classes );
		}
	}
}