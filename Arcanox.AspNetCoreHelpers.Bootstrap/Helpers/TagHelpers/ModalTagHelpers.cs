﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Bootstrap.Helpers.TagHelpers
{
	[ HtmlTargetElement( "b-modal" ) ]
	[ RestrictChildren( "b-modal-header", "b-modal-body", "b-modal-footer" ) ]
	public class ModalTagHelper : SimpleTagWithClassesTagHelper
	{
		public ModalTagHelper() : base( "modal" ) { }

		public override void Process( TagHelperContext context, TagHelperOutput output )
		{
			base.Process( context, output );

			output.PreContent
				  .AppendHtml( @"<div class=""modal-dialog"">" )
				  .AppendHtml( @"<div class=""modal-content"">" );

			output.PostContent
				  .AppendHtml( @"</div>" )
				  .AppendHtml( @"</div>" );
		}
	}

	[ HtmlTargetElement( "b-modal-header" ) ]
	public class ModalHeaderTagHelper : SimpleTagWithClassesTagHelper
	{
		public ModalHeaderTagHelper() : base( "modal-header" ) { }
	}

	[ HtmlTargetElement( "b-modal-body" ) ]
	public class ModalBodyTagHelper : SimpleTagWithClassesTagHelper
	{
		public ModalBodyTagHelper() : base( "modal-body" ) { }
	}

	[ HtmlTargetElement( "b-modal-footer" ) ]
	public class ModalFooterTagHelper : SimpleTagWithClassesTagHelper
	{
		public ModalFooterTagHelper() : base( "modal-footer" ) { }
	}
}