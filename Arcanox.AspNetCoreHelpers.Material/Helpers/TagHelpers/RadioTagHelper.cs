﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Material.Helpers.TagHelpers
{
	public class RadioTagHelper : TagHelper
	{
		public ModelExpression AspFor { get; set; }
		public object Value { get; set; }

		public override async Task ProcessAsync( TagHelperContext context, TagHelperOutput output )
		{
			var childContent = await output.GetChildContentAsync();

			output.PreElement
				  .AppendHtml( @"<div class=""radio"">" )
				  .AppendHtml( @"<label>" );

			output.TagName = "input";
			output.Attributes.Add( "id", this.AspFor.Name );
			output.Attributes.Add( "name", this.AspFor.Name );
			output.Attributes.Add( "type", "radio" );
			output.Attributes.Add( "value", this.Value );

			if ( this.AspFor.Model?.Equals( this.Value ) == true )
				output.Attributes.Add( "checked", "checked" );

			output.TagMode = TagMode.StartTagAndEndTag;

			if ( childContent.IsEmptyOrWhiteSpace )
				output.PostElement.AppendHtml( $@" {this.AspFor.Metadata.GetDisplayName()}" );
			else
				output.PostElement.AppendHtml( childContent );

			output.PostElement
				  .AppendHtml( @"</label>" )
				  .AppendHtml( @"</div>" );
		}
	}
}