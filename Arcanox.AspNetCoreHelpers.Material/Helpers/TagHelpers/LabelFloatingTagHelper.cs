﻿using Arcanox.AspNetCoreHelpers.Bootstrap.Helpers.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Material.Helpers.TagHelpers
{
	[ HtmlTargetElement( "label", Attributes = "[floating]" ) ]
	public class LabelFloatingTagHelper : SimpleTagWithClassesTagHelper
	{
		public LabelFloatingTagHelper() : base( "bmd-label-floating" ) { }
	}
}