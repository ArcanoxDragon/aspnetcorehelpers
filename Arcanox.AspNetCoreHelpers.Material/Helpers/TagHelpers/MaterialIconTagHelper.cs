﻿using Arcanox.AspNetCoreHelpers.Extensions;
using MaterialDesign.Icons;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Material.Helpers.TagHelpers
{
	[ HtmlTargetElement( "icon" ) ]
	public class MaterialIconTagHelper : TagHelper
	{
		[ HtmlAttributeName( "name" ) ]
		public MaterialIcon Icon { get; set; }

		public override void Process( TagHelperContext context, TagHelperOutput output )
		{
			output.TagName = "span";
			output.TagMode = TagMode.StartTagAndEndTag;
			output.Attributes.PrependClasses( "material-icons" );
			output.Content.SetContent( this.Icon.ToIconFontString() );
		}
	}
}