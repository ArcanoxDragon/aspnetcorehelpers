﻿using System.Threading.Tasks;
using MaterialDesign.Icons;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Material.Helpers.TagHelpers
{
	public class CheckboxTagHelper : TagHelper
	{
		private readonly IHtmlHelper html;

		public CheckboxTagHelper( IHtmlHelper html )
		{
			this.html = html;
		}

		[ ViewContext ] public ViewContext ViewContext { get; set; }

		public ModelExpression AspFor         { get; set; }
		public bool            HideLabel      { get; set; }
		public MaterialIcon    Icon           { get; set; } = MaterialIcon.Unknown;
		public string          FormGroupClass { get; set; } = "";

		public override async Task ProcessAsync( TagHelperContext context, TagHelperOutput output )
		{
			( (IViewContextAware) this.html ).Contextualize( this.ViewContext );

			var childContent = await output.GetChildContentAsync();
			var @checked     = this.AspFor?.Model as bool? == true;

			output.Attributes.RemoveAll( "form-group-class" );
			output.PreElement.AppendHtml( $@"<div class=""bmd-form-group form-group-checkbox {this.FormGroupClass}"">" );

			if ( this.Icon != MaterialIcon.Unknown )
			{
				output.PreElement
					  .AppendHtml( @"<div class=""input-group-icon input-group-checkbox"">" )
					  .AppendHtml( this.html.MaterialIcon( this.Icon ) );
			}

			output.PreElement
				  .AppendHtml( @"<div class=""checkbox"">" )
				  .AppendHtml( @"<label>" );

			output.TagName = "input";
			output.TagMode = TagMode.StartTagAndEndTag;
			output.Attributes.Add( "type",  "checkbox" );
			output.Attributes.Add( "value", "true" );

			if ( this.AspFor != null )
			{
				output.Attributes.Add( "id",   this.AspFor.Name.Replace( '.', '_' ) );
				output.Attributes.Add( "name", this.AspFor.Name );
			}

			if ( @checked )
			{
				output.Attributes.Add( "checked", "checked" );
			}

			if ( this.HideLabel )
			{
				output.PostElement.AppendHtml( "&nbsp;" );
			}
			else
			{
				if ( childContent.IsEmptyOrWhiteSpace && this.AspFor != null )
					output.PostElement.AppendHtml( $@" {this.AspFor.Metadata.GetDisplayName()}" );
				else
					output.PostElement.AppendHtml( childContent );
			}

			output.PostElement
				  .AppendHtml( @"</label>" )
				  .AppendHtml( @"</div>" );

			if ( this.Icon != MaterialIcon.Unknown )
			{
				output.PostElement
					  .AppendHtml( "</div>" )
					  .AppendHtml( this.AspFor == null ? null : this.html.ValidationMessage( this.AspFor.Name, null, new { @class = "invalid-feedback" } ) );
			}

			output.PostElement.AppendHtml( "</div>" );
		}
	}
}