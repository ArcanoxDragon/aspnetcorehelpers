﻿using System.Collections.Generic;
using System.Linq;
using Arcanox.AspNetCoreHelpers.Extensions;
using MaterialDesign.Icons;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Arcanox.AspNetCoreHelpers.Material.Helpers.TagHelpers
{
	public enum MaterialInputLabelMode
	{
		None,
		Floating,
		Static
	}

	[ HtmlTargetElement( "input", Attributes    = "[material]" ) ]
	[ HtmlTargetElement( "input", Attributes    = "[icon]" ) ]
	[ HtmlTargetElement( "input", Attributes    = "[label]" ) ]
	[ HtmlTargetElement( "textarea", Attributes = "[material]" ) ]
	[ HtmlTargetElement( "textarea", Attributes = "[icon]" ) ]
	[ HtmlTargetElement( "textarea", Attributes = "[label]" ) ]
	public class MaterialInputTagHelper : TagHelper
	{
		private readonly IHtmlHelper    html;
		private readonly IHtmlGenerator generator;

		public MaterialInputTagHelper( IHtmlHelper html,
									   IHtmlGenerator generator )
		{
			this.html      = html;
			this.generator = generator;
		}

		[ ViewContext ] public ViewContext ViewContext { get; set; }

		public MaterialIcon           Icon         { get; set; } = MaterialIcon.Unknown;
		public MaterialInputLabelMode Label        { get; set; } = MaterialInputLabelMode.Floating;
		public ModelExpression        AspFor       { get; set; }
		public bool                   HideRequired { get; set; }

		public override void Process( TagHelperContext context, TagHelperOutput output )
		{
			( (IViewContextAware) this.html ).Contextualize( this.ViewContext );

			var inputType    = output.Attributes[ "type" ]?.Value.ToString().ToLower() ?? "text";
			var ourLabelType = this.Label == MaterialInputLabelMode.Floating ? "floating" : "static";
			var labelClass = inputType.StartsWith( "date" )
								 ? "bmd-label-static"
								 : $"bmd-label-{ourLabelType}";
			var hasError   = false;
			var isRequired = this.AspFor?.Metadata.IsRequired == true;
			var isFilled   = this.AspFor?.Model?.ToString().IsNullOrEmpty() == false;

			if ( context.AllAttributes.TryGetAttribute( "value", out var valueAttribute ) &&
				 valueAttribute.Value?.ToString().IsNullOrEmpty() == false )
				isFilled = true;

			if ( this.AspFor != null && this.ViewContext.ModelState.TryGetValue( this.AspFor.Name, out var modelState ) )
			{
				if ( modelState.Errors.Any() )
					hasError = true;
			}

			var classes = new List<string> { "form-group", "bmd-form-group" };

			if ( hasError )
				classes.Add( "has-error" );
			if ( isRequired )
				classes.Add( "is-required" );
			if ( isFilled )
				classes.Add( "is-filled" );
			if ( this.HideRequired )
				classes.Add( "hide-required" );

			output.PreElement.AppendHtml( $@"<div class=""{classes.Join( " " )}"">" );

			if ( this.Icon != MaterialIcon.Unknown )
				output.PreElement
					  .AppendHtml( $@"<div class=""input-group-icon has-label-{ourLabelType}"">" )
					  .AppendHtml( this.html.MaterialIcon( this.Icon ) );

			output.Attributes.AppendClasses( "form-control" );

			if ( this.Label != MaterialInputLabelMode.None )
			{
				output.PostElement.AppendHtml( this.html.Label( this.AspFor?.Name,
																this.AspFor?.Metadata.GetDisplayName(),
																new { @class = labelClass } ) );
			}

			if ( this.Icon != MaterialIcon.Unknown )
				output.PostElement.AppendHtml( "</div>" );

			if ( this.AspFor != null )
			{
				var validationSpan = new TagBuilder( "span" ) { TagRenderMode = TagRenderMode.Normal };

				validationSpan.AddCssClass( "invalid-feedback" );

				var validationBuilder = this.generator.GenerateValidationMessage(
					this.ViewContext,
					this.AspFor.ModelExplorer,
					this.AspFor.Name,
					message: null,
					tag: null,
					htmlAttributes: null );

				if ( validationBuilder != null )
				{
					validationSpan.MergeAttributes( validationBuilder.Attributes );

					if ( validationBuilder.HasInnerHtml )
						validationSpan.InnerHtml.AppendHtml( validationBuilder );
				}

				output.PostElement.AppendHtml( validationSpan );
			}

			output.PostElement
				  .AppendHtml( "</div>" );
		}
	}
}