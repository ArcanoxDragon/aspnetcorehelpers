﻿using System;
using System.Linq.Expressions;
using MaterialDesign.Icons;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Arcanox.AspNetCoreHelpers.Material.Helpers
{
	public static class MaterialHelpers
	{
		private static IHtmlContent IconControlFor<TModel, TResult>( IHtmlHelper<TModel> html,
																	 Expression<Func<TModel, TResult>> expression,
																	 MaterialIcon icon,
																	 IHtmlContent control )
			=> new HtmlContentBuilder()
			   .AppendHtml( @"<div class=""form-icon-holder"">" )
			   /**/.AppendHtml( html.MaterialIcon( icon ) )
			   /**/.AppendHtml( @"<div class=""form-group bmd-form-group"">" )
			   /******/.AppendHtml( html.LabelFor( expression, new { @class = "bmd-label-floating" } ) )
			   /******/.AppendHtml( control )
			   /******/.AppendHtml( html.ValidationMessageFor( expression, null, new { @class = "invalid-feedback" } ) )
			   /**/.AppendHtml( @"</div>" )
			   .AppendHtml( @"</div>" );

		public static IHtmlContent IconTextBoxFor<TModel, TResult>( this IHtmlHelper<TModel> html,
																	Expression<Func<TModel, TResult>> expression,
																	MaterialIcon icon )
			=> MaterialHelpers.IconControlFor( html, expression, icon,
											   html.TextBoxFor( expression, new { @class = "form-control" } ) );

		public static IHtmlContent IconPasswordFor<TModel, TResult>( this IHtmlHelper<TModel> html,
																	 Expression<Func<TModel, TResult>> expression,
																	 MaterialIcon icon )
			=> MaterialHelpers.IconControlFor( html, expression, icon,
											   html.PasswordFor( expression, new { @class = "form-control" } ) );

		public static IHtmlContent MaterialIcon( this IHtmlHelper html, MaterialIcon icon )
			=> html.Raw( $@"<span class=""material-icons"">{icon.ToIconFontString()}</span>" );
	}
}