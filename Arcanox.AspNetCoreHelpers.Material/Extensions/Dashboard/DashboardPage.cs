﻿using Arcanox.AspNetCoreHelpers.Features.Dashboard.Models;
using MaterialDesign.Icons;

namespace Arcanox.AspNetCoreHelpers.Material.Extensions.Dashboard
{
    public static class DashboardPageExtensions
	{
		public static DashboardPage WithIcon( this DashboardPage page, MaterialIcon icon )
			=> page.WithIcon( icon.ToIconFontString() );
	}
}
